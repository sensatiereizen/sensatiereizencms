<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Contracts\CategoryContract;
use App\Contracts\ProductContract;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\Category;
use App\Models\Brand;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryContract $categoryRepository, ProductContract $productRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
    }

    public function show($slug)
    {

        $category = $this->categoryRepository->findBySlug($slug);
        $pagination = 2;
        
        $brands = Brand::all();
        

        return view('site.pages.category', compact('category', 'brands'));
    }

    public function showmain()
    {

        $categories = $this->categoryRepository->listCategories();
    

        return view('site.pages.soccercategorypage')->with('categories', $categories);
    }


    public function links() {
        $categories = $this->categoryRepository->listCategories();
      
        return  view('site.pages.homepage')->with('categories', $categories);
    }

    public function search(Request $request)
    {
        $request->validate([
            'search' => 'required|min:3',
        ]);

        $query = $request->input('filter');

        // $products = Product::where('name', 'like', "%$query%")
        //                    ->orWhere('details', 'like', "%$query%")
        //                    ->orWhere('description', 'like', "%$query%")
        //                    ->paginate(10);

        $products = Product::search($query)->paginate(10);

        return view('site.pages.search-results')->with('products', $products);
    }

}
