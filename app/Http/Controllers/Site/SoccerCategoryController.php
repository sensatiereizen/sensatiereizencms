<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\CategoryContract;
use App\Contracts\ProductContract;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\Category;
use App\Models\Brand;
use Cart;

class SoccerCategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryContract $categoryRepository, ProductContract $productRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
    }

    public function show($slug)
    {

        $category = $this->categoryRepository->findBySlug($slug);
        $pagination = 2;
        
        
       
        if (request()->category) { 
            $products = Product::with('categories')->whereHas('categories', function ($query) {
                $query->whereIn('id', request('category'));
             });
        };
        if (request()->brand) {
            $products = Product::when($brand, function ($query, $brand) {
                return $query->where('brand_id', $brand);
            });
        };
        if (request()->accomodation) {
            $products = ProductAttribute::where('id','=',2)->where('id', request('size'))->OrderBy('id', 'desc')->paginate($pagination);           
        };
        if (request()->color) {
            $products = ProductAttribute::where('id','=',2)->where('id', request('color'))->OrderBy('id', 'desc')->paginate($pagination);
                        
        };
        if (request()->sort == 'low_high') {
            $products = Product::orderBy('price')->paginate($pagination);
        } elseif (request()->sort == 'high_low') {
            $products = Product::orderBy('price', 'desc')->paginate($pagination);
        } 
           

        return view('site.pages.soccercategory', compact('category'));
    }

    public function showmain()
    {

        $categories = $this->categoryRepository->listSoccerCategories();

        return view('site.pages.soccercategorypage')->with('categories', $categories);
    }

    public function showgolfmain()
    {

        $categories = $this->categoryRepository->listGolfCategories();

        return view('site.pages.golfcategorypage')->with('categories', $categories);
    }

    public function indexFiltering(Request $request)
{
    $filter = $request->query('filter');
    $pagination = 2;

        // $products = Product::sortable()
        //     ->where('products.name', 'like', '%'.$filter.'%')
        //     ->paginate(5);
        $products = ProductAttribute::where('id','=',2)->where('id', request('size'))->OrderBy('id', 'desc')->paginate($pagination);           



    return view('site.pages.category')->with('products', $products)->with('filter', $filter);
    }

    public function links() {
        $categories = $this->categoryRepository->listCategories();
      
        return  view('site.pages.homepage')->with('categories', $categories);
    }

    public function search(Request $request)
    {
        $request->validate([
            'search' => 'required|min:3',
        ]);

        $query = $request->input('filter');

        // $products = Product::where('name', 'like', "%$query%")
        //                    ->orWhere('details', 'like', "%$query%")
        //                    ->orWhere('description', 'like', "%$query%")
        //                    ->paginate(10);

        $products = Product::search($query)->paginate(10);

        return view('site.pages.search-results')->with('products', $products);
    }


    public function filter(Request $request)
    {
        $request->validate([
            'filter' => 'required|min:3',
        ]);

        $query = $request->input('filter');

        $products = Product::whereHas('product_attributes', function ($query) {
            $query->where('name', 'R')
                ->whereIn('value', ['8GB', '4GB']);
        })->whereHas('attributes', function ($query) {
            $query->where('name', 'HDD')
                ->where('value', '256GB');
        })->get();

        return view('site.pages.soccercategory')->with('products', $products);
    }

    public function addToCart(Request $request)
    {
        $product = $this->productRepository->findProductById($request->input('productId'));
        $options = $request->except('_token', 'productId', 'price', 'qty');

        Cart::clear();

        Cart::add($request->input('productId'), $product->name, $request->input('price'), 1, $options);

        return  view('site.pages.checkout')->with('products', $product);
    }

}
