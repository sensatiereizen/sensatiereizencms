@extends('admin.app')
@section('title') {{ $pageTitle }} @endsection
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/js/plugins/dropzone/dist/min/dropzone.min.css') }}"/>
@endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-shopping-bag"></i> {{ $pageTitle }} - {{ $subTitle }}</h1>
        </div>
    </div>
    @include('admin.partials.flash')
    <div class="row user">
        <div class="col-md-3">
            <div class="tile p-0">
                <ul class="nav flex-column nav-tabs user-tabs">
                    <li class="nav-item"><a class="nav-link active" href="#general" data-toggle="tab">General</a></li>
                    <li class="nav-item"><a class="nav-link" href="#images" data-toggle="tab">Images/Video</a></li>
                    <li class="nav-item"><a class="nav-link" href="#attributes" data-toggle="tab">Attributes</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-content">
                <div class="tab-pane active" id="general">
                    <div class="tile">
                        <form action="{{ route('admin.products.update') }}" enctype="multipart/form-data" method="POST" role="form">
                            @csrf
                            <h3 class="tile-title">Product Information  <a href="javascript:history.back()" class="btn btn-default">Back</a></h3>
                            <hr>
                            <div class="tile-body">
                                <div class="form-group">
                                    <label class="control-label" for="name">Name</label>
                                    <input
                                        class="form-control @error('name') is-invalid @enderror"
                                        type="text"
                                        placeholder="Enter attribute name"
                                        id="name"
                                        name="name"
                                        value="{{ old('name', $product->name) }}"
                                    />
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                    <div class="invalid-feedback active">
                                        <i class="fa fa-exclamation-circle fa-fw"></i> @error('name') <span>{{ $message }}</span> @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="sku">Slug</label>
                                            <input
                                                class="form-control @error('sku') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter product sku"
                                                id="sku"
                                                name="sku"
                                                value="{{ old('sku', $product->sku) }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('sku') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="brand_id">Filter</label>
                                            <select name="brand_id" id="brand_id" class="form-control @error('brand_id') is-invalid @enderror">
                                                <option value="0">Select</option>
                                                @foreach($brands as $brand)
                                                    @if ($product->brand_id == $brand->id)
                                                        <option value="{{ $brand->id }}" selected>{{ $brand->name }}</option>
                                                    @else
                                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('brand_id') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="categories">Categories</label>
                                            <select name="categories[]" id="categories" class="form-control" multiple>
                                                @foreach($categories as $category)
                                                    @php $check = in_array($category->id, $product->categories->pluck('id')->toArray()) ? 'selected' : ''@endphp
                                                    <option value="{{ $category->id }}" {{ $check }}>{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="price">Price</label>
                                            <input
                                                class="form-control @error('price') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter product price"
                                                id="price"
                                                name="price"
                                                value="{{ old('price', $product->price) }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('price') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="sale_price">Special Price</label>
                                            <input
                                                class="form-control"
                                                type="text"
                                                placeholder="Enter product special price"
                                                id="sale_price"
                                                name="sale_price"
                                                value="{{ old('sale_price', $product->sale_price) }}"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="quantity">Quantity</label>
                                            <input
                                                class="form-control @error('quantity') is-invalid @enderror"
                                                type="number"
                                                placeholder="Enter product quantity"
                                                id="quantity"
                                                name="quantity"
                                                value="{{ old('quantity', $product->quantity) }}"
                                            />
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('quantity') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="weight">Weight</label>
                                            <input
                                                class="form-control"
                                                type="text"
                                                placeholder="Enter product weight"
                                                id="weight"
                                                name="weight"
                                                value="{{ old('weight', $product->weight) }}"
                                            />
                                        </div>
                                    </div> -->
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="description">Description</label>
                                    <textarea name="description" id="description" rows="8" class="form-control">{{ old('description', $product->description) }}</textarea>
                                </div>
                                <!-- <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            @if ($product->image != null)
                                                <figure class="mt-2" style="width: 80px; height: auto;">
                                                    <img src="{{ url('storage/'.$product->image) }}" id="image" class="img-fluid" alt="img">
                                                </figure>
                                            @endif
                                        </div>
                                        <div class="col-md-10">
                                            <label class="control-label">Product Main Image</label>
                                            <input class="form-control @error('image') is-invalid @enderror" type="file" id="image" name="image"/>
                                            @error('image') {{ $message }} @enderror
                                        </div>
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   id="status"
                                                   name="status"
                                                   {{ $product->status == 1 ? 'checked' : '' }}
                                                />Status
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   id="featured"
                                                   name="featured"
                                                   {{ $product->featured == 1 ? 'checked' : '' }}
                                                />Featured
                                        </label>
                                    </div>
                                </div>
                                <h3 class="tile-title">UPC</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="upc1">UPC 1</label>
                                            <input
                                                class="form-control @error('upc1') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter upc"
                                                id="upc1"
                                                name="upc1"
                                                value="{{ old('upc1', $product->upc1) }}"
                                            />
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('upc1') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                            <label class="control-label" for="upc2">UPC 2</label>
                                            <input
                                                class="form-control @error('upc2') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter upc"
                                                id="upc2"
                                                name="upc2"
                                                value="{{ old('upc2', $product->upc2) }}"
                                            />
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('upc2') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="upc3">UPC 3</label>
                                            <input
                                                class="form-control @error('upc3') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter upc"
                                                id="upc3"
                                                name="upc3"
                                                value="{{ old('upc3', $product->upc3) }}"
                                            />
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('upc3') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                            <label class="control-label" for="upc4">UPC 4</label>
                                            <input
                                                class="form-control @error('upc4') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter upc"
                                                id="upc4"
                                                name="upc4"
                                                value="{{ old('upc4', $product->upc4) }}"
                                            />
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('upc4') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <h3 class="tile-title">Digit</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label" for="digit1">Digit 1</label>
                                            <input
                                                class="form-control @error('digit1') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter digit"
                                                id="digit1"
                                                name="digit1"
                                                value="{{ old('digit1', $product->digit1) }}"
                                            />
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('digit1') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    <div class="form-group">
                                            <label class="control-label" for="digit2">Digit 2</label>
                                            <input
                                                class="form-control @error('digit2') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter upc"
                                                id="digit2"
                                                name="digit2"
                                                value="{{ old('digit2', $product->digit2) }}"
                                            />
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('digit2') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    <div class="form-group">
                                            <label class="control-label" for="digit3">Digit 3</label>
                                            <input
                                                class="form-control @error('digit3') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter upc"
                                                id="digit3"
                                                name="digit3"
                                                value="{{ old('digit3', $product->digit3) }}"
                                            />
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('digit3') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="tile-title">Basic Information</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="product_basic_title">Basis informatie</label>
                                            <input
                                                class="form-control @error('product_basic_title') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter attribute name"
                                                id="product_basic_title"
                                                name="product_basic_title"
                                                value="{{ old('product_basic_title', $product->product_basic_title) }}"
                                            />
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('product_basic_title') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label" for="product_basic_description">Basis Description</label>
                                        <textarea name="product_basic_description" id="product_basic_description" rows="8" class="form-control">{{ old('product_basic_description', $product->product_basic_description) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="product_basic_titlesecond">Basis informatie 1</label>
                                            <input
                                                class="form-control @error('product_basic_titlesecond') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter attribute name"
                                                id="product_basic_titlesecond"
                                                name="product_basic_titlesecond"
                                                value="{{ old('product_basic_titlesecond', $product->product_basic_titlesecond) }}"
                                            />
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('product_basic_titlesecond') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label" for="product_basic_descriptionsecond">Basis Description 1</label>
                                        <textarea name="product_basic_descriptionsecond" id="product_basic_descriptionsecond" rows="8" class="form-control">{{ old('product_basic_descriptionsecond', $product->product_basic_descriptionsecond) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="product_basic_titlethird">Basis informatie 2</label>
                                            <input
                                                class="form-control @error('product_basic_titlethird') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter attribute name"
                                                id="product_basic_titlethird"
                                                name="product_basic_titlethird"
                                                value="{{ old('product_basic_titlethird', $product->product_basic_titlethird) }}"
                                            />
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('product_basic_titlethird') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label" for="product_basic_descriptionthird">Basis Description 2</label>
                                        <textarea name="product_basic_descriptionthird" id="product_basic_descriptionthird" rows="8" class="form-control">{{ old('product_basic_descriptionthird', $product->product_basic_descriptionthird) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="product_basic_titlefourth">Basis informatie 3</label>
                                            <input
                                                class="form-control @error('product_basic_titlefourth') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter attribute name"
                                                id="product_basic_titlefourth"
                                                name="product_basic_titlefourth"
                                                value="{{ old('product_basic_titlefourth', $product->product_basic_titlefourth) }}"
                                            />
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('product_basic_titleofourth') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label" for="product_basic_descriptionfourth">Basis Description 3</label>
                                        <textarea name="product_basic_descriptionfourth" id="product_basic_descriptionfourth" rows="8" class="form-control">{{ old('product_basic_descriptionfourth', $product->product_basic_descriptionfourth) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="product_basic_titlefifth">Basis informatie 4</label>
                                            <input
                                                class="form-control @error('product_basic_titlefifth') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter attribute name"
                                                id="product_basic_titlefifth"
                                                name="product_basic_titlefifth"
                                                value="{{ old('product_basic_titlefifth', $product->product_basic_titlefifth) }}"
                                            />
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('product_basic_titleofifth') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label" for="product_basic_descriptionfifth">Basis Description 4</label>
                                        <textarea name="product_basic_descriptionfifth" id="product_basic_descriptionfifth" rows="8" class="form-control">{{ old('product_basic_descriptionfifth', $product->product_basic_descriptionfifth) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="product_basic_titlesix">Basis informatie 5</label>
                                            <input
                                                class="form-control @error('product_basic_titlesix') is-invalid @enderror"
                                                type="text"
                                                placeholder="Enter attribute name"
                                                id="product_basic_titlesix"
                                                name="product_basic_titlesix"
                                                value="{{ old('product_basic_titlesix', $product->product_basic_titlesix) }}"
                                            />
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('product_basic_titleosix') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label" for="product_basic_descriptionsix">Basis Description 5</label>
                                        <textarea name="product_basic_descriptionsix" id="product_basic_descriptionsix" rows="8" class="form-control">{{ old('product_basic_descriptionsix', $product->product_basic_descriptionsix) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tile-footer">
                                <div class="row d-print-none mt-2">
                                    <div class="col-12 text-right">
                                        <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Product</button>
                                        <a class="btn btn-danger" href="{{ route('admin.products.index') }}"><i class="fa fa-fw fa-lg fa-arrow-left"></i>Go Back</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="images">
                    <div class="tile">
                        <h3 class="tile-title">Upload File</h3>
                        <hr>
                        <div class="tile-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="" class="dropzone" id="dropzone" style="border: 2px dashed rgba(0,0,0,0.3)">
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </div>
                            <div class="row d-print-none mt-2">
                                <div class="col-12 text-right">
                                    <button class="btn btn-success" type="button" id="uploadButton">
                                        <i class="fa fa-fw fa-lg fa-upload"></i>Upload Images
                                    </button>
                                </div>
                            </div>
                            @if ($product->images)
                                <hr>
                                <div class="row">
                                    @foreach($product->images as $image)
                                        <div class="col-md-3">
                                            <div class="card">
                                                <div class="card-body">
                                                    <img src="{{ asset('storage/'.$image->full) }}" id="brandLogo" class="img-fluid" alt="img">
                                                    <a class="card-link float-right text-danger" href="{{ route('admin.products.images.delete', $image->id) }}">
                                                        <i class="fa fa-fw fa-lg fa-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="video">
                    <div class="tile">
                        <h3 class="tile-title">Upload Video</h3>
                        <hr>
                        <div class="tile-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="" class="dropzone" id="dropzone" style="border: 2px dashed rgba(0,0,0,0.3)">
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </div>
                            <div class="row d-print-none mt-2">
                                <div class="col-12 text-right">
                                    <button class="btn btn-success" type="button" id="uploadButton">
                                        <i class="fa fa-fw fa-lg fa-upload"></i>Upload Video
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="attributes">
                    <product-attributes :productid="{{ $product->id }}"></product-attributes>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('backend/js/plugins/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/dropzone/dist/min/dropzone.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/plugins/bootstrap-notify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/app.js') }}"></script>
    <script>
        Dropzone.autoDiscover = false;

        $( document ).ready(function() {
            $('#categories').select2();

            let myDropzone = new Dropzone("#dropzone", {
                paramName: "image",
                addRemoveLinks: false,
                maxFilesize: 100,
                parallelUploads: 7,
                uploadMultiple: false,
                url: "{{ route('admin.products.images.upload') }}",
                autoProcessQueue: false,
            });
            myDropzone.on("queuecomplete", function (file) {
                window.location.reload();
                showNotification('Completed', 'All product images uploaded', 'success', 'fa-check');
            });
            $('#uploadButton').click(function(){
                if (myDropzone.files.length === 0) {
                    showNotification('Error', 'Please select files to upload.', 'danger', 'fa-close');
                } else {
                    myDropzone.processQueue();
                }
            });
            function showNotification(title, message, type, icon)
            {
                $.notify({
                    title: title + ' : ',
                    message: message,
                    icon: 'fa ' + icon
                },{
                    type: type,
                    allow_dismiss: true,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                });
            }
        });
    </script>
@endpush
