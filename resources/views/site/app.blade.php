<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') - {{ config('app.name') }}</title>
    @include('site.partials.styles')
</head>
<body>
<!-- <div class="preloader" id="pageLoad">
		<div class="holder">
			<div class="coffee_cup"></div>
		</div>
</div> -->
	<!-- main wrapper -->
	<div id="wrapper">
		<div class="page-wrapper">
            @include('site.partials.header')
            @yield('content')
    </div>
    @include('site.partials.footer')
    </div>

    <div class="scroll-holder text-center">
		<a href="javascript:" id="scroll-to-top"><i class="icon-arrow-down"></i></a>
	</div>
    @include('site.partials.scripts')
</body>
</html>
