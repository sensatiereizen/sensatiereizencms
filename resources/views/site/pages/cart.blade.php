@extends('site.app')
@section('title', 'Offerte Aanvraag')
@section('content')
<section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" id="gridview-sidebar">
            <div class="banner-text">
					<div class="center-text">
						<div class="container">
							<h1>Samenvatting</h1>
							<!-- breadcrumb -->
							<nav class="breadcrumbs">
								<ul>
									<li><a href="#">HOME</a></li>
									<li><a>Samenvatting</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
</section>
    <main id="main">
            <div class="row">
                <div class="col-sm-12">
                    @if (Session::has('message'))
                        <p class="alert alert-success">{{ Session::get('message') }}</p>
                    @endif
                </div>
            </div>
				<div class="inner-main common-spacing container">
					<!-- cart information holder -->
					<div class="cart-holder table-container">
						<div class="table-responsive">
                        @if (\Cart::isEmpty())
                            <p class="alert alert-warning">Your shopping cart is empty.</p>
                        @else
							<table class="table table-hover table-align-right">
								<thead>
									<tr>
										<th>
											&nbsp;
										</th>
										<th>
											<strong class="date-text">Geselecteerde GolfResort</strong>
											<!-- <span class="sub-text">Confirmed Dates</span> -->
										</th>
										<th>
											<strong class="date-text">Prijs</strong>
											<!-- <span class="sub-text">Updated Today</span> -->
										</th>
										<th>
											<strong class="date-text">Aantal Personen</strong>
											<!-- <span class="sub-text">Including Children</span> -->
										</th>
										<th>
											<strong class="date-text">Totaal Prijs</strong>
											<!-- <span class="sub-text">Excluding Flights</span> -->
										</th>
									</tr>
								</thead>
								<tbody>
                                @foreach(\Cart::getContent() as $item)
									<tr>
										<td>
											<div class="cell">
												<div class="middle">
													<a class="delete" href="{{ route('checkout.cart.remove', $item->id) }}"><span class="icon-trash"></span></a>
												</div>
											</div>
										</td>
										<td>
											<div class="cell">
												<div class="middle">
													<div class="info">
														<div class="img-wrap">
															<img src="img/listing/img-40.jpg" height="240" width="350" alt="image description">
														</div>
														<div class="text-wrap">
															<strong class="product-title">{{ $item->name }}</strong>
															<!-- <time class="time" datetime="2016-11-05">14th Jan 2016</time> -->
														</div>
													</div>
												</div>
											</div>
										</td>
										<td>
											<div class="cell">
												<div class="middle">
													<span class="price">{{ config('settings.currency_symbol'). $item->price }}</span>
												</div>
											</div>
										</td>
										<td>
											<div class="cell">
												<div class="middle">
													<div class="num-hold">
														<a href="#" class="minus control"><span class="icon-minus-normal"></span></a>
														<a href="#" class="plus control"><span class="icon-plus-normal"></span></a>
														<span class="val">{{ $item->quantity }}</span>
													</div>
												</div>
											</div>
										</td>
										<td>
											<div class="cell">
												<div class="middle">
													<span class="price">{{ config('settings.currency_symbol') }}{{ \Cart::getSubTotal() }}</span>
												</div>
											</div>
										</td>
									</tr>
                                    @endforeach
								</tbody>
							</table>
                            @endif
						</div>
						<div class="cart-option">
							<div class="coupon-hold">
								<!-- <div class="submit-wrap">
									<button class="btn btn-default" type="submit">APPLY COUPON</button>
								</div> -->
								<!-- <div class="input-hold">
									<input type="text" class="form-control" placeholder="Enter Coupon Code.....">
								</div> -->
							</div>
							<div class="button-hold">
								<a href="{{ route('checkout.index') }}" class="btn btn-default">CHECKOUT</a>
								<!-- <a href="#" class="btn btn-default">UPDATE CART</a> -->
							</div>
						</div>
					</div>
				</div>
			</main>
@stop
