@extends('site.app')
@section('title', 'Order Completed')
@section('content')
<section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" id="gridview-sidebar">
				<div class="banner-text">
					<div class="center-text">
						<div class="container">
							<h1>Bevestiging van uw Golfreis</h1>
							<!-- breadcrumb -->
							<nav class="breadcrumbs">
								<ul>
									<li><a href="#">HOME</a></li>
									<li><a>Bevestiging</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
</section>
    <main id="main">
				<!-- confirmation block -->
				<div class="inner-main common-spacing confirmation-block">
					<div class="container">
						<h2>Bedankt voor uw Order.</h2>
						<p><span>Wij gaan ermee aan de slag Ordernummer: </span><span>{{ $order->order_number }}.</span></p>
					</div>
				</div>
	</main>
@stop
