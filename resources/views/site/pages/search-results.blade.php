@extends('site.app')

@section('content')


<div class="search-results-container container">
        <h1>Search Results</h1>
        <p class="search-results-count">{{ $products->total() }} result(s) for '{{ request()->input('query') }}'</p>
</div>
<main id="main">
				<!-- content with sidebar -->
				<div class="bg-gray content-with-sidebar grid-view-sidebar">
					<div class="container">
						<div id="two-columns" class="row">
							<!-- sidebar -->
							<aside id="sidebar" class="col-md-4 col-lg-3 sidebar sidebar-list">
								<div class="sidebar-holder">
									<header class="heading">
										<h3>FILTER</h3>
									</header>
									<div class="accordion">
										<div class="accordion-group">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" href="#collapse1" aria-expanded="true" aria-controls="collapse1">SELECT REGION</a>
												</h4>
											</div>
											<div id="collapse1" class="panel-collapse collapse in" role="tabpanel">
												<div class="panel-body">
													<ul class="side-list region-list hovered-list">
														<li>
															<a href="#">
																<span class="ico-holder">
																	<span class="icon-asia"></span>
																</span>
																<span class="text">Frankrijk</span>
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="accordion-group">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" href="#collapse2" aria-expanded="true" aria-controls="collapse2">Activity type</a>
												</h4>
											</div>
											<div id="collapse2" class="panel-collapse collapse in" role="tabpanel">
												<div class="panel-body">
													<ul class="side-list horizontal-list hovered-list">
														<li>
															<a href="#">
																<span class="icon-hiking-camping"></span>
																<span class="popup">
																	Hiking
																</span>
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="accordion-group">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" href="#collapse3" aria-expanded="true" aria-controls="collapse3">Landscape</a>
												</h4>
											</div>
											<div id="collapse3" class="panel-collapse collapse in" role="tabpanel">
												<div class="panel-body">
													<ul class="side-list horizontal-list hovered-list">
														<li>
															<a href="#">
																<span class="icon-beach"></span>
																<span class="popup">
																	beach
																</span>
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="accordion-group">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" href="#collapse4" aria-expanded="true" aria-controls="collapse4">Activity level</a>
												</h4>
											</div>
											<div id="collapse4" class="panel-collapse collapse in" role="tabpanel">
												<div class="panel-body">
													<ul class="side-list horizontal-list hovered-list">
														<li>
															<a href="#">
																<span class="icon-level1"></span>
																<span class="popup">
																	Level 1
																</span>
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="accordion-group">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" href="#collapse5" aria-expanded="true" aria-controls="collapse5">Price range</a>
												</h4>
											</div>
											<div id="collapse5" class="panel-collapse collapse in" role="tabpanel">
												<div class="panel-body">
													<div id="slider-range"></div>
													<input type="text" id="amount" readonly class="price-input">
													<div id="ammount" class="price-input">
														
													</div>
												</div>
											</div>
										</div>
										<div class="accordion-group">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" href="#collapse6" aria-expanded="true" aria-controls="collapse6">community poll</a>
												</h4>
											</div>
											<div id="collapse6" class="panel-collapse collapse in" role="tabpanel">
												<div class="panel-body">
													<strong class="title">What shoes do your prefer on hiking trips?</strong>
													<ul class="side-list check-list">
														<li>
															<label for="check1" class="custom-checkbox">
																<input id="check1" type="checkbox">
																<span class="check-input"></span>
																<span class="check-label">Hiking Boots</span>
															</label>
														</li>
													</ul>
													<strong class="sub-link"><a href="#">CAST YOUR VOTE</a></strong>
												</div>
											</div>
										</div>
									</div>
								</div>
							</aside>
							<div id="content" class="col-md-8 col-lg-9">
								<div class="filter-option filter-box">
									<strong class="result-info">{{count($products)}} GOLFRESORTS</strong>
									<!-- <div class="layout-holder">
										<div class="layout-action">
											<a href="#" class="link link-list"><span class="icon-list"></span></a>
											<a href="#" class="link link-grid active"><span class="icon-grid"></span></a>
										</div>
										<div class="select-holder">
											<div class="select-col">
												<select class="filter-select sort-select">
													<option value="sort">Sort Order</option>
													<option value="sort">Price</option>
													<option value="sort">Recent</option>
													<option value="sort">Popular</option>
												</select>
											</div>
										</div>
									</div> -->
								</div>
								<div class="content-holder content-sub-holder">
									<div class="row db-3-col">
                                    @forelse($products as $product)
                                    <a href="{{ route('product.show', $product->slug) }}">
										<article class="col-md-6 col-lg-4 article has-hover-s1 thumb-full">
                                            <div class="thumbnail">
       
                                                <h3 class="small-space"><a href="{{ route('product.show', $product->slug) }}">{{ $product->name }}</a></h3>
                                                <aside class="meta">
                                                </aside>
                                                <p>{{ $product->description }}</p>
                                                <a href="{{ route('product.show', $product->slug) }}" class="btn btn-default">MEER INFO</a>
                                                <footer>
                                                    @if ($product->sale_price != 0)
                                                        <span class="price">vanaf <span>{{ config('settings.currency_symbol').$product->sale_price }}</span></span>
                                                        <span class="price-old">vanaf <span>{{ config('settings.currency_symbol').$product->price }}</span></span>
                                                    @else
                                                        <span class="price">vanaf <span>{{ config('settings.currency_symbol').$product->price }}</span></span>
                                                    @endif
                                                </footer>
                                            </div>
                                        </article>
                                    </a>
                                    @empty
                                        <p>No Products found</p>
                                    @endforelse
									</div>
								</div>
								<!-- pagination wrap -->
								<!-- <nav class="pagination-wrap">
									<div class="btn-prev">
										<a href="#" aria-label="Previous">
											<span class="icon-angle-right"></span>
										</a>
									</div>
									<ul class="pagination">
										<li><a href="#">1</a></li>
										<li><a href="#">2</a></li>
										<li><a href="#">3</a></li>
										<li class="active"><a href="#">4</a></li>
										<li><a href="#">5</a></li>
										<li>...</li>
										<li><a href="#">7</a></li>
									</ul>
									<div class="btn-next">
										<a href="#" aria-label="Previous">
											<span class="icon-angle-right"></span>
										</a>
									</div>
								</nav> -->
							</div>
						</div>
					</div>
				</div>
				<!-- recent block -->
				<aside class="recent-block recent-list recent-wide-thumbnail">
					<div class="container">
						<h2 class="text-center text-uppercase">RECENTLY VIEWED</h2>
						<div class="row">
							<article class="col-sm-6 col-md-3 article">
								<div class="thumbnail">
									<h3 class="no-space"><a href="#">Everest Basecamp Trek</a></h3>
									<strong class="info-title">Everest Region, Nepal</strong>
									<div class="img-wrap">
										<img src="img/listing/img-31.jpg" height="210" width="250" alt="image description">
									</div>
									<footer>
										<div class="sub-info">
											<span>5 Days</span>
											<span>$299</span>
										</div>
										<ul class="ico-list">
											<li class="pop-opener">
												<a href="#">
													<span class="icon-hiking"></span>
													<span class="popup">
														Hiking
													</span>
												</a>
											</li>
											<li class="pop-opener">
												<a href="#">
													<span class="icon-mountain"></span>
													<span class="popup">
														Mountain
													</span>
												</a>
											</li>
											<li class="pop-opener">
												<a href="#">
													<span class="icon-level5"></span>
													<span class="popup">
														Level 5
													</span>
												</a>
											</li>
										</ul>
									</footer>
								</div>
							</article>
							<article class="col-sm-6 col-md-3 article">
								<div class="thumbnail">
									<h3 class="no-space"><a href="#">Everest Basecamp Trek</a></h3>
									<strong class="info-title">Everest Region, Nepal</strong>
									<div class="img-wrap">
										<img src="img/listing/img-32.jpg" height="210" width="250" alt="image description">
									</div>
									<footer>
										<div class="sub-info">
											<span>5 Days</span>
											<span>$299</span>
										</div>
										<ul class="ico-list">
											<li class="pop-opener">
												<a href="#">
													<span class="icon-hiking"></span>
													<span class="popup">
														Hiking
													</span>
												</a>
											</li>
											<li class="pop-opener">
												<a href="#">
													<span class="icon-mountain"></span>
													<span class="popup">
														Mountain
													</span>
												</a>
											</li>
											<li class="pop-opener">
												<a href="#">
													<span class="icon-level5"></span>
													<span class="popup">
														Level 5
													</span>
												</a>
											</li>
										</ul>
									</footer>
								</div>
							</article>
							<article class="col-sm-6 col-md-3 article">
								<div class="thumbnail">
									<h3 class="no-space"><a href="#">Everest Basecamp Trek</a></h3>
									<strong class="info-title">Everest Region, Nepal</strong>
									<div class="img-wrap">
										<img src="img/listing/img-33.jpg" height="210" width="250" alt="image description">
									</div>
									<footer>
										<div class="sub-info">
											<span>5 Days</span>
											<span>$299</span>
										</div>
										<ul class="ico-list">
											<li class="pop-opener">
												<a href="#">
													<span class="icon-hiking"></span>
													<span class="popup">
														Hiking
													</span>
												</a>
											</li>
											<li class="pop-opener">
												<a href="#">
													<span class="icon-mountain"></span>
													<span class="popup">
														Mountain
													</span>
												</a>
											</li>
											<li class="pop-opener">
												<a href="#">
													<span class="icon-level5"></span>
													<span class="popup">
														Level 5
													</span>
												</a>
											</li>
										</ul>
									</footer>
								</div>
							</article>
							<article class="col-sm-6 col-md-3 article">
								<div class="thumbnail">
									<h3 class="no-space"><a href="#">Everest Basecamp Trek</a></h3>
									<strong class="info-title">Everest Region, Nepal</strong>
									<div class="img-wrap">
										<img src="img/listing/img-34.jpg" height="210" width="250" alt="image description">
									</div>
									<footer>
										<div class="sub-info">
											<span>5 Days</span>
											<span>$299</span>
										</div>
										<ul class="ico-list">
											<li class="pop-opener">
												<a href="#">
													<span class="icon-hiking"></span>
													<span class="popup">
														Hiking
													</span>
												</a>
											</li>
											<li class="pop-opener">
												<a href="#">
													<span class="icon-mountain"></span>
													<span class="popup">
														Mountain
													</span>
												</a>
											</li>
											<li class="pop-opener">
												<a href="#">
													<span class="icon-level5"></span>
													<span class="popup">
														Level 5
													</span>
												</a>
											</li>
										</ul>
									</footer>
								</div>
							</article>
						</div>
					</div>
				</aside>
			</main>
@stop
