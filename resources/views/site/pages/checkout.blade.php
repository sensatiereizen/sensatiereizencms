@extends('site.app')
@section('title', 'Checkout')
@section('content')
<section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" id="gridview-sidebar">
            <div class="banner-text">
					<div class="center-text">
						<div class="container">
							<h1>Checkout</h1>
							<!-- breadcrumb -->
							<nav class="breadcrumbs">
								<ul>
									<li><a href="#">HOME</a></li>
									<li><a>Checkout</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
</section>
   

	<main id="main">

				<div class="inner-main common-spacing container">
					<!-- booking form -->
					<div class="row">
                <div class="col-sm-12">
                    @if (Session::has('error'))
                        <p class="alert alert-danger">{{ Session::get('error') }}</p>
                    @endif
                </div>
            </div>
					<form class="booking-form" action="{{ route('checkout.place.order') }}" method="POST" role="form">
						<div class="row">
							<div class="col-md-6">
							<div class="form-holder">
									<h2 class="small-size">Billing Information</h2>
									<div class="wrap">
										<div class="row">
											<div class="col-md-6">
												<div class="hold">
													<label for="name">First Name</label>
													<input type="text" id="first_name" name="first_name" class="form-control">
												</div>
											</div>
											<div class="col-md-6">
												<div class="hold">
													<label for="lname">Last Name</label>
													<input type="text" id="last_name" name="last_name" class="form-control">
												</div>
											</div>
										</div>
										<!-- <div class="hold">
											<label for="cname">Company Name</label>
											<input type="text" id="cname" class="form-control">
										</div> -->
										<div class="hold">
											<label for="address">Address</label>
											<input type="text" id="address" name="address" class="form-control">
										</div>
										<div class="hold">
											<label for="city">City</label>
											<input type="text" id="city" name="city" name="city" class="form-control">
										</div>
                                        <div class="hold">
											<label for="city">Country</label>
											<input type="text" id="country" name="country" class="form-control">
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="hold">
													<label for="em">Email address</label>
													<input type="email" id="email" name="email" class="form-control">
												</div>
											</div>
											<div class="col-md-6">
												<div class="hold">
													<label for="phn">Phone</label>
													<input type="text" id="phone_number" name="phone_number" class="form-control">
												</div>
											</div>
										</div>
										<ul class="option">
											<li>
												<header class="title">
													<label class="custom-radio">
														<input type="radio" name="pay">
														<span class="check-input"></span>
														<span class="check-label">Check Payment</span>
													</label>
												</header>
												<div class="info-hold">
													<p>Please send your cheque to Entrada Inc. Address, Thank You!</p>
												</div>
											</li>
											<li>
												<header class="title">
													<label class="custom-radio">
														<input type="radio" name="pay">
														<span class="check-input"></span>
														<span class="check-label">Paypal Payment</span>
													</label>
												</header>
												<div class="info-hold">
													<p>If you dont have paypal account - you can still pay using credit cards!</p>
												</div>
											</li>
										</ul> 
										<ul class="payment-option">
											<li>
												<img alt="visa" src="img/footer/visa.png">
											</li>
											<li>
												<img width="33" height="20" alt="master card" src="img/footer/mastercard.png">
											</li>
											<li>
												<img width="72" height="20" alt="paypal" src="img/footer/paypal.png">
											</li>
											<li>
												<img width="33" height="20" alt="maestro" src="img/footer/maestro.png">
											</li>
											<li>
												<img width="55" height="20" alt="bank transfer" src="img/footer/bank-transfer.png">
											</li>
										</ul> 
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-holder">
									<h2 class="small-size">Additional Notes</h2>
									<div class="wrap">
										<div class="hold">
											<label for="txt">Your Comment</label>
											<textarea id="txt" class="form-control" name="notes" placeholder="Please enter any additional information here, eg. food/drug requirements etc."></textarea>
										</div>
									</div>
									<div class="order-block">
										<h2 class="small-size">Preview Order</h2>
										<div class="wrap">
											<table class="product-table">
												<thead>
													<tr>
														<th>Resort</th>
														<th>Totaal Prijs</th>
													</tr>
												</thead>
												<tbody>
												@foreach(\Cart::getContent() as $item)
													<tr>
														<td>
															<span class="title">{{ $item->name }} &emsp;<span class="amount">x&emsp; {{ $item->quantity }}</span></span>
														</td>
														<td>
															<span class="amount">{{ number_format($item->price, 2, ',', '.') }}</span>
														</td>
													</tr>
												</tbody>
												<tfoot>
													<tr>
														<td>Totaal</td>
														<td>{{ number_format(\Cart::getSubTotal(), 2, ',', '.') }}</td>
													</tr>
												</tfoot>
												@endforeach
											</table>
											<button type="submit" class="btn btn-default">Verzenden</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</main>

@stop
