@extends('site.app')
@section('title', 'Homepage')
@section('content')
<div class="banner banner-home">
<div id="rev_slider_70_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="web-product-light-hero66" style="margin:0px auto;background-color:#474d4b;padding:0px;margin-top:0px;margin-bottom:0px;">
					<div id="rev_slider_70_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
						<ul>
							<li data-index="rs-81" data-transition="slideoverup" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-rotate="0"  data-saveperformance="off"  data-title="The Menu" data-description="">
								<!-- main image for revolution slider -->
								<img alt="image description" src="{{ asset('frontend/images/banner/GS-Achtergrond-Golfreizen-Homepage.png') }}" height="1280" width="1920"  data-lazyload="img/banner/img-04.jpg" data-bgposition="right center" data-kenburns="on" data-duration="30000" data-ease="Power1.easeOut" data-scalestart="110" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>

								<div class="tp-caption tp-resizeme" id="slide-897-layer-7" 
									data-x="['left','left','center','center']" 
									data-hoffset="['20','40','0','0']" 
									data-y="['middle','middle','top','top']" 
									data-voffset="['-160','-158','100','50']" 
									data-width="none" 
									data-height="none" 
									data-whitespace="nowrap" 
									data-transform_idle="o:1;" 
									data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power2.easeInOut;" 
									data-transform_out="opacity:0;s:300;s:300;" 
									data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
									data-start="1500" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on"
									style="z-index: 9; white-space: nowrap; font-size: 60px; line-height: 100px;text-align:center;">
									<img src="{{ asset('frontend/images/icons/Homepage-Golfer-Icon.png') }}" height="100" width="100">
								</div>

								<div class="tp-caption banner-heading-sub tp-resizeme  toblur rs-parallaxlevel-10" id="slide-81-layer-1" 
									data-x="['left','left','center','center']" 
									data-hoffset="['20','40','0','0']" 
									data-y="['top','top','top','top']" 
									data-voffset="['230',180','220','140']" 
									data-fontsize="['60','60','0','0']"
									data-lineheight="['60','60','40','40']"
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeOut;" 
									data-transform_out="opacity:0;s:1000;s:1000;" 
									data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
									data-start="700" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 
									data-visibility=”[‘on’,’on’,’off’,’off’]” 
									style="z-index: 5; white-space: nowrap; font-size: 60px; font-weight: 900; line-height: 60px;">VIND
								</div>

								<div class="tp-caption RotatingWords-TitleWhite  tp-resizeme  tounblur rs-parallaxlevel-10" id="slide-333-layer-11"
									data-x="['left','left','center','center']" 
									data-hoffset="['170','190','0','0']" 
									data-y="['top','top','top','top']" 
									data-voffset="['230',180','180','150']"
									data-fontsize="['60','60','40','40']"
									data-lineheight="['60','60','40','40']"
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_in="sX:0.8;sY:0.8;opacity:0;s:500;e:Power4.easeOut;" 
									data-transform_out="sX:0.9;sY:0.9;opacity:0;s:500;e:Power3.easeIn;s:500;e:Power3.easeIn;" 
									data-start="1500" 
									data-splitin="chars" 
									data-splitout="chars" 
									data-responsive_offset="on" 
									data-elementdelay="0.1" 
									data-endelementdelay="0.03" 
									data-end="4000" 
									style="z-index: 7; white-space: nowrap; font-size: 60px; font-weight: 900; line-height: 60px;">UW UNIEKE GOLFREIS!
								</div>

								<div class="tp-caption RotatingWords-TitleWhite   tp-resizeme  tounblur rs-parallaxlevel-10" id="slide-333-layer-13" 
									data-x="['left','left','center','center']" 
									data-hoffset="['170','190','0','0']" 
									data-y="['top','top','top','top']" 
									data-voffset="['230',180','180','150']"  
									data-fontsize="['60','60','40','40']"
									data-lineheight="['60','60','40','40']"
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_in="sX:0.8;sY:0.8;opacity:0;s:500;e:Power3.easeOut;" 
									data-transform_out="sX:0.9;sY:0.9;opacity:0;s:500;e:Power3.easeIn;s:500;e:Power3.easeIn;" 
									data-start="4510" 
									data-splitin="chars" 
									data-splitout="chars" 
									data-responsive_offset="on" 
									data-elementdelay="0.1" 
									data-endelementdelay="0.03" 
									data-end="7010" 
									style="z-index: 8; white-space: nowrap; font-size: 60px; font-weight: 900; line-height: 60px;">UW LUXE RESORT!
								</div>

								<div class="tp-caption RotatingWords-TitleWhite   tp-resizeme  tounblur rs-parallaxlevel-10" id="slide-333-layer-14" 
									data-x="['left','left','center','center']" 
									data-hoffset="['170','190','0','0']" 
									data-y="['top','top','top','top']" 
									data-voffset="['230',180','180','150']" 
									data-fontsize="['60','60','40','40']"
									data-lineheight="['60','60','40','40']"
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_in="sX:0.8;sY:0.8;opacity:0;s:500;e:Power3.easeOut;" 
									data-transform_out="sX:0.9;sY:0.9;opacity:0;s:500;e:Power3.easeIn;s:500;e:Power3.easeIn;" 
									data-start="7490" 
									data-splitin="chars" 
									data-splitout="chars" 
									data-responsive_offset="on" 
									data-elementdelay="0.1" 
									data-endelementdelay="0.03" 
									data-end="9990" 
									style="z-index: 9; white-space: nowrap; font-size: 60px; font-weight: 900; line-height: 60px;">UW ULTIEME RUST! 
								</div>
								<div class="tp-caption RotatingWords-TitleWhite   tp-resizeme  tounblur rs-parallaxlevel-10" id="slide-333-layer-15" 
									data-x="['left','left','center','center']" 
									data-hoffset="['170','190','0','0']" 
									data-y="['top','top','top','top']" 
									data-voffset="['230',180','180','150']"
									data-fontsize="['60','60','40','40']"
									data-lineheight="['60','60','40','40']"
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;"
									data-transform_in="sX:0.8;sY:0.8;opacity:0;s:500;e:Power3.easeOut;" 
									data-transform_out="sX:0.9;sY:0.9;opacity:0;s:500;e:Power3.easeIn;s:500;e:Power3.easeIn;" 
									data-start="10490" 
									data-splitin="chars" 
									data-splitout="chars" 
									data-responsive_offset="on" 
									data-elementdelay="0.1" 
									data-endelementdelay="0.03" 
									style="z-index: 10; white-space: nowrap; font-size: 60px; font-weight: 900; line-height: 60px;">UW JUISTE SWING!
								</div>
							</li>						
						</ul>
					</div>
				</div>
				<div class="banner-text">
					<div class="center-text">
						<form class="trip-form">
							<fieldset>
								<div class="holder">
									<label for="adventure">Selecteer Het Land</label>
									<div class="select-holder">
										<select class="trip" id="adventure">
											<option>Scuba Diving</option>
											<option>Beach Party</option>
											<option>Family Fun</option>
											<option>Couples Den</option>
											<option>Island Trips</option>
											<option>Boating Sailing</option>
										</select>
									</div>
								</div>
								<div class="holder">
									<label for="destination">Select Het Resort</label>
									<div class="select-holder">
										<select class="trip" id="destination">
											<option>Europe - Spain</option>
											<option>Europe - Greece</option>
											<option>Africa - Kenya</option>
											<option>Africa - Angola</option>
											<option>Asia - Vietnam</option>
											<option>Mid East - Dubai</option>
										</select>
									</div>
								</div>
								<div class="holder">
									<label for="date">Selecteer De Datum</label>
									<div class="select-holder">
										<select class="trip" id="date">
											<option>Jan - Mar 2016</option>
											<option>Apr - Jun 2016</option>
											<option>Jul - Sep 2016</option>
											<option>Oct - Dec 2016</option>
										</select>
									</div>
								</div>
								<div class="holder">
									<input class="btn btn-trip" type="submit" value="let's go!">
								</div>
							</fieldset>
						</form> 
					</div>
				</div>
			</div>

			<!-- main container -->

			<main id="main">
				<!-- special block -->
				<aside class="special-block">
					<div class="container-xl">
						<p class="special-text">Stel uw vraag snel en eenvoudig via <strong>E-MAIL: <a href="mail:info@sensatiereizen.nl">info@sensatiereizen.nl</a></strong></p>
					</div>
				</aside>
				<div class="featured-content adventure-holder">
					<div class="container-fluid">
						<div class="row same-height">
							<div class="col-md-6 image height wow slideInLeft">
								<div class="bg-stretch">
									<img src="{{ asset('frontend/images/country/Golfreizen-Spanje-Homepage.jpg') }}" height="627" width="960" alt="image description">
								</div>
							</div>
							<div class="col-md-6 text-block height wow slideInRight">
								<div class="centered">
									<h2 class="intro-heading">Golfreizen | Spanje</h2>
									<p class="intro">De Spaanse cultuur staat de Nederlander of Belg altijd erg aan. Geniet van heerlijke tapas op een terras aan de golfbaan van de meest luxe resorts in heel Spanje. Pak een moment voor uzelf en ervaar de kwaliteit van leven.</p>
									<a href="category/4" class="btn btn-primary btn-lg">Geniet!</a>
								</div>
							</div>
						</div>
						<div class="row same-height">
							<div class="col-md-6 image height wow slideInRight">
								<div class="bg-stretch">
									<img src="{{ asset('frontend/images/country/Golfreizen-Portugal-Homepage.jpg') }}" height="627" width="960" alt="image description">
								</div>
							</div>
							<div class="col-md-6 text-block height wow slideInLeft">
								<div class="centered">
								<h2 class="intro-heading">Golfreizen | Portugal</h2>
									<p class="intro">Dit waanzinnige land is bij uitstek een golf favoriet te noemen. Met name in de Portugese Algarve is het voor de golf liefhebber een ware sensatie te noemen. Met de meest indrukwekkende banen is hier voor ieder wat wils.</p>
									<a href="category/3" class="btn btn-primary btn-lg">Ervaar!</a>
								</div>
							</div>
						</div>
						<div class="row same-height">
							<div class="col-md-6 image height wow slideInLeft">
								<div class="bg-stretch">
									<img src="{{ asset('frontend/images/country/Golfreizen-Italië-Homepage.jpg') }}" height="627" width="960" alt="image description">
								</div>
							</div>
							<div class="col-md-6 text-block height wow slideInRight">
								<div class="centered">
									<h2 class="intro-heading">Golfreizen | Italië</h2>
									<p class="intro">Het ò zo mooie Italië is bij uitstek één van de top bestemmingen in ons golfreizen aanbod. Met name de regio Toscane is zeer geliefd bij de wat meer ervaren golfers. La dolce vita in een perfect klimaat en courses.</p>
									<a href="category/5" class="btn btn-primary btn-lg">Beleef!</a>
								</div>
							</div>
						</div>
						<div class="row same-height">
							<div class="col-md-6 image height wow slideInRight">
								<div class="bg-stretch">
									<img src="{{ asset('frontend/images/country/Golfreizen-Canarische-Eilanden-Homepage.jpg') }}" height="627" width="960" alt="image description">
								</div>
							</div>
							<div class="col-md-6 text-block height wow slideInLeft">
								<div class="centered">
									<h2 class="intro-heading">Golfreizen | Canarische-Eilanden</h2>
									<p class="intro">Het golfen op één van de Canarische Eilanden is een must voor de golfliefhebber. Het is een andere beleving dan op het vaste land, met banen die een ligging hebben in het zeeklimaat, altijd zon en vind de ultieme rust.</p>
									<a href="category/6" class="btn btn-primary btn-lg">Ontdek!</a>
								</div>
							</div>
						</div>
						<div class="row same-height">
							<div class="col-md-6 image height wow slideInLeft">
								<div class="bg-stretch">
									<img src="{{ asset('frontend/images/country/Golfreizen-Frankrijk-Homepage.jpg') }}" height="627" width="960" alt="image description">
								</div>
							</div>
							<div class="col-md-6 text-block height wow slideInRight">
								<div class="centered">
									<h2 class="intro-heading">Golfreizen | Frankrijk</h2>
									<p class="intro">Het Franse leven is iets wat niet mag ontbreken voor golf reizigers. De schoonheid van dit land ervaren in absolute luxe en stilte. Werk aan uw unieke swing en geniet van de meest heerlijke Franse delicatessen.</p>
									<a href="category/2" class="btn btn-primary btn-lg">Besef!</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- article list holder -->
				<div class="content-block content-spacing">
					<div class="container">
						<header class="content-heading">
							<h2 class="main-heading">MEEST GEBOEKT | TOP RESORTS</h2>
							<span class="main-subtitle">Ga direct naar de favoriete verblijfplaatsen van onze golfreis klanten!</span>
							<div class="seperator"></div>
						</header>
						<div class="content-holder">
							<div class="row db-3-col">
								<article class="col-sm-6 col-md-4 article has-hover-s3">
									<div class="img-wrap">
										<a href="product/pga-catalunya-resort-5">
											<img src="{{ asset('frontend/images/top-resorts/Golfreizen-Resort-Homepage-1.jpg') }}" height="215" width="370" alt="image description">
										</a>
				
										<div class="hover-article">
											<div class="star-rating">
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
											</div>
											<div class="icons">
												<a href="/product/castelfalfi-resort-5"><span class="icon-reply"></span></a>
											</div>
											<div class="info-footer">
												<span class="price">vanaf <span>€459,- P.P.</span></span>
												<a href="/product/castelfalfi-resort-5" class="link-more">BEKIJK</a>
											</div>
										</div>
									</div>
									<h3 style="text-align:center;"><a href="product/pga-catalunya-resort-5">Castelfalfi Golf Resort 5*</a></h3>
								</article>
								<article class="col-sm-6 col-md-4 article has-hover-s3">
									<div class="img-wrap">
										<a href="/product/terre-blanche-spa-golf-5">
											<img src="{{ asset('frontend/images/top-resorts/Golfreizen-Resort-Homepage-2.jpg') }}"height="215" width="370" alt="image description">
										</a>
						
										<div class="hover-article">
											<div class="star-rating">
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
											</div>
											<div class="icons">
												<a href="/product/terre-blanche-spa-golf-5"><span class="icon-reply"></span></a>
											</div>
											<div class="info-footer">
												<span class="price">vanaf <span>€575,- P.P.</span></span>
												<a href="/product/terre-blanche-spa-golf-5" class="link-more">BEKIJK</a>
											</div>
										</div>
									</div>
									<h3 style="text-align:center;"><a href="/product/terre-blanche-spa-golf-5">Terre Blanche Spa & Golf 5*</a></h3>
								</article>
								<article class="col-sm-6 col-md-4 article has-hover-s3">
									<div class="img-wrap">
										<a href="/product/pga-catalunya-resort-5">
											<img src="{{ asset('frontend/images/top-resorts/Golfreizen-Resort-Homepage-3.jpg') }}" height="215" width="370" alt="image description">
										</a>
						
										<div class="hover-article">
											<div class="star-rating">
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
											</div>
											<div class="icons">
												<a href="/product/pga-catalunya-resort-5"><span class="icon-reply"></span></a>
											</div>
											<div class="info-footer">
												<span class="price">vanaf <span>€399,- P.P.</span></span>
												<a href="/product/pga-catalunya-resort-5" class="link-more">BEKIJK</a>
											</div>
										</div>
									</div>
									<h3 style="text-align:center;"><a href="/product/pga-catalunya-resort-5">PGA Catalunya Resort 5*</a></h3>
								</article>
								<article class="col-sm-6 col-md-4 article has-hover-s3">
									<div class="img-wrap">
										<a href="/product/penha-longa-resort-5">
											<img src="{{ asset('frontend/images/top-resorts/Golfreizen-Resort-Homepage-4.jpg') }}" height="215" width="370" alt="image description">
										</a>
						
										<div class="hover-article">
											<div class="star-rating">
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
											</div>
											<div class="icons">
												<a href="/product/penha-longa-resort-5"><span class="icon-reply"></span></a>
											</div>
											<div class="info-footer">
												<span class="price">vanaf <span>€343,- P.P.</span></span>
												<a href="/product/penha-longa-resort-5" class="link-more">BEKIJK</a>
											</div>
										</div>
									</div>
									<h3 style="text-align:center;"><a href="/product/penha-longa-resort-5">Penha Longa Golfresort 5*</a></h3>
								</article>
								<article class="col-sm-6 col-md-4 article has-hover-s3">
									<div class="img-wrap">
										<a href="product/salobre-resort-serenity-5">
											<img src="{{ asset('frontend/images/top-resorts/Golfreizen-Resort-Homepage-5.jpg') }}" height="215" width="370" alt="image description">
										</a>
						
										<div class="hover-article">
											<div class="star-rating">
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
											</div>
											<div class="icons">
												<a href="product/salobre-resort-serenity-5"><span class="icon-reply"></span></a>
											</div>
											<div class="info-footer">
												<span class="price">vanaf <span>€489,- P.P.</span></span>
												<a href="product/salobre-resort-serenity-5" class="link-more">BEKIJK</a>
											</div>
										</div>
									</div>
									<h3 style="text-align:center;"><a href="product/salobre-resort-serenity-5">Salobre Serenity Resort 5*</a></h3>
								</article>
								<article class="col-sm-6 col-md-4 article has-hover-s3">
									<div class="img-wrap">
										<a href="/product/verdura-golf-spa-5">
											<img src="{{ asset('frontend/images/top-resorts/Golfreizen-Resort-Homepage-6.jpg') }}" height="215" width="370" alt="image description">
										</a>
					
										<div class="hover-article">
											<div class="star-rating">
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
											</div>
											<div class="icons">
												<a href="/product/verdura-golf-spa-5"><span class="icon-reply"></span></a>
											</div>
											<div class="info-footer">
												<span class="price">vanaf <span>€519,- P.P.</span></span>
												<a href="/product/verdura-golf-spa-5" class="link-more">BEKIJK</a>
											</div>
										</div>
									</div>
									<h3 style="text-align:center;"><a href="/product/verdura-golf-spa-5">Verdura Golf & Spa Resort 5*</a></h3>
								</article>
							</div>
						</div>
					</div>
				</div>
				<!-- couter block -->
                <aside class="count-block">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-6 col-md-4 block-1">
                                <div class="holder">
                                <span style="font-size: 40px;" class="fas fa-thumbs-up"></span>

                                    <span class="info"><span class="counter">5000</span>+</span>
                                    <span class="txt">SR AMBASSADEURS</span>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-4 block-2">
                                <div class="holder">
                                <span style="font-size: 40px;" class="fas fa-globe-europe"></span>

                                    <span class="info"><span class="counter">10</span></span>
                                    <span class="txt">LANDEN (Z.S.M)</span>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-4 block-3">
                                <div class="holder">
                                <span style="font-size: 40px;" class="fas fa-golf-ball"></span>

                                    <span class="info"><span class="counter">100</span></span>
                                    <span class="txt">GOLF RESORTS</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
				<!-- featured adventure content -->

				<!-- browse block -->
				<div class="browse-block">
					<div class="browse-destination column">
						<a><span>WIJ BLIJVEN GROEIEN</span></a>
					</div>
					<div class="browse-adventures column">
						<a><span>ANDERS DAN ANDEREN</span></a>
					</div>
				</div>
				<!-- partner list -->
				<article class="partner-block">
					<div class="container">
						<header class="content-heading">
							<h2 class="main-heading">SensatieReizen</h2>
							<span class="main-subtitle">People who always support and endorse our good work</span>
							<div class="seperator"></div>
						</header>
						<div class="partner-list" id="partner-slide">
							<div class="partner">
								<a href="#">
									<img width="130" src="{{ asset('frontend/images/partners/SR-Partnership-1.png') }}" alt="image description">
									<img class="hover" width="130" src="{{ asset('frontend/images/partners/SR-Partnership-1.png') }}" alt="image description">
								</a>
							</div>
							<div class="partner">
								<a href="#">
									<img width="130" src="{{ asset('frontend/images/partners/SR-Partnership-2.png') }}" alt="image description">
									<img class="hover" width="130" src="{{ asset('frontend/images/partners/SR-Partnership-2.png') }}" alt="image description">
								</a>
							</div>
							<div class="partner">
								<a href="#">
									<img width="130" src="{{ asset('frontend/images/partners/SR-Partnership-3.png') }}" alt="image description">
									<img class="hover" width="130" src="{{ asset('frontend/images/partners/SR-Partnership-3.png') }}" alt="image description">
								</a>
							</div>
							<div class="partner">
								<a href="#">
									<img width="130" src="{{ asset('frontend/images/partners/SR-Partnership-4.png') }}" alt="image description">
									<img class="hover" width="130" src="{{ asset('frontend/images/partners/SR-Partnership-4.png') }}" alt="image description">
								</a>
							</div>
							<div class="partner">
								<a href="#">
									<img width="130" src="{{ asset('frontend/images/partners/SR-Partnership-5.png') }}" alt="image description">
									<img class="hover" width="130" src="{{ asset('frontend/images/partners/SR-Partnership-5.png') }}" alt="image description">
								</a>
							</div>
							<div class="partner">
								<a href="#">
									<img width="130" src="{{ asset('frontend/images/partners/SR-Partnership-6.png') }}" alt="image description">
									<img class="hover" width="130" src="{{ asset('frontend/images/partners/SR-Partnership-6.png') }}" alt="image description">
								</a>
							</div>
							<div class="partner">
								<a href="#">
									<img width="130" src="{{ asset('frontend/images/partners/SR-Partnership-7.png') }}" alt="image description">
									<img class="hover" width="130" src="{{ asset('frontend/images/partners/SR-Partnership-7.png') }}" alt="image description">
								</a>
							</div>
							<div class="partner">
								<a href="#">
									<img width="130" src="{{ asset('frontend/images/partners/SR-Partnership-8.png') }}" alt="image description">
									<img class="hover" width="130" src="{{ asset('frontend/images/partners/SR-Partnership-8.png') }}" alt="image description">
								</a>
							</div>
							<div class="partner">
								<a href="#">
									<img width="130" src="{{ asset('frontend/images/partners/SR-Partnership-9.png') }}" alt="image description">
									<img class="hover" width="130" src="{{ asset('frontend/images/partners/SR-Partnership-9.png') }}" alt="image description">
								</a>
							</div>
						</div>
					</div>
				</article>
			</main>
@stop
