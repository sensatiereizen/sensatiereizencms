@extends('site.app')
@section('title', $product->name)
@section('content')
			<!-- main banner -->
			<div class="banner banner-home">
			
				<!-- revolution slider starts -->
				<div id="rev_slider_279_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="restaurant-header" style="margin:0px auto;background-color:#474d4b;padding:0px;margin-top:0px;margin-bottom:0px;">
					<div id="rev_slider_70_1" class="rev_slider fullscreenabanner" style="display:none;" data-version="5.1.4">
						<ul>
							<li class="slider-color-schema-dark" data-index="rs-2" data-transition="fade" data-slotamount="7" data-easein="default" data-easeout="default" data-masterspeed="1000" data-rotate="0" data-saveperformance="off" data-title="Slide" data-description="">
								<!-- main image for revolution slider -->
								@if ($product->images->count() > 0)
									<img src="{{ asset('storage/'.$product->images->first()->full) }}" alt="image description" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-bgfit="cover" data-no-retina>

                                    @else
                                        <div class="img-big-wrap">
                                            <div>
                                                <a href="https://via.placeholder.com/176" data-fancybox=""><img src="https://via.placeholder.com/176"></a>
                                            </div>
                                        </div>
                                    @endif

								<div class="tp-caption tp-resizeme" id="slide-897-layer-7" 
									data-x="['center','center','center','center']" 
									data-hoffset="['0','0','0','0']" 
									data-y="['top','top','middle','middle']" 
									data-voffset="['160','120','-120','-70']" 
									data-width="none" 
									data-height="none" 
									data-whitespace="nowrap" 
									data-transform_idle="o:1;" 
									data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power2.easeInOut;" 
									data-transform_out="opacity:0;s:300;s:300;" 
									data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
									data-start="1500" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 
									style="z-index: 9; white-space: nowrap; font-size: 60px; line-height: 100px;text-align:center;">
									<!-- <span class="icon-wildlife"></span> -->
             
								</div>

								<div class="tp-caption banner-heading-sub tp-resizeme rs-parallaxlevel-0" 
									data-x="['center','center','center','center']" 
									data-hoffset="['0','0','0','0']" 
									data-y="['top','top','middle','middle']" 
									data-voffset="['280','240','10','20']" 
									data-fontsize="['48','48','44','28']"
									data-lineheight="['85','85','50','50']" 
									data-width="['1200','1000','750','480']" 
									data-height="none" 
									data-whitespace="normal" 
									data-transform_idle="o:1;" 
									data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
									data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									data-mask_in="x:0px;y:0px;" 
									data-mask_out="x:inherit;y:inherit;" 
									data-start="1000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 
									style="z-index: 7; letter-spacing: 0; font-weight: 100; text-align: center; color: #ffffff">{{ $product->name }}
								</div>

								<div class="tp-caption banner-heading-sub tp-resizeme rs-parallaxlevel-10"  
									data-x="['center','center','center','center']" 
									data-hoffset="['0','0','0','0']" 
									data-y="['top','top','middle','middle']" 
									data-voffset="['340','290','70','70']" 
									data-fontsize="['60','60','60','40']" 
									data-lineheight="['110','110','100','60']"
									data-width="none"
									data-height="none"
									data-whitespace="nowrap"
									data-transform_idle="o:1;" 
									data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" 
									data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
									data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
									data-start="1000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" 
									style="z-index: 8; padding-right: 10px; text-indent: 5px; font-weight: 900; white-space: nowrap;">{{ config('settings.currency_symbol') }}{{ $product->price }}

								</div>
								<form action="" method="POST" role="form" id="addToCart">
								@csrf
									<div class="tp-caption rev-btn  rs-parallaxlevel-10" id="slide-163-layer-2" 
										data-x="['center','center','center','center']" 
										data-hoffset="['0','0','0','0']" 
										data-y="['middle','middle','middle','middle']" 
										data-voffset="['150','160','180','150']" 
										data-width="none"
										data-height="none"
										data-whitespace="nowrap"
										data-transform_idle="o:1;"
										data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power3.easeOut;"
										data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
										data-transform_out="y:[175%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
										data-mask_out="x:inherit;y:inherit;" 
										data-start="1250" 
										data-splitin="none" 
										data-splitout="none" 
										data-actions='[{"event":"click","action":"jumptoslide","slide":"rs-164","delay":""}]'
										data-responsive_offset="on"> 
								
										<!-- <button type="submit" class="btn btn-banner">OFFERTE</button> -->
									</div>
								</form>
							</li>
						</ul>
					</div>
				</div>

				<div class="feature-block">
					<div class="holder">
						<ul>
							<li>
								<a>
									<span class="ico">
										<!-- <span class="icon-checkmark"></span> -->
                                        <i class="fas fa-check"></i>
									</span>
									<span class="info">{{ $product->upc1 }}</span>
								</a>
							</li>
							<li>
								<a>
									<span class="ico">
                                    <i class="fas fa-check"></i>
									</span>
									<span class="info">{{ $product->upc2 }}</span>
								</a>
							</li>
							<li>
								<a>
									<span class="ico">
                                    <i class="fas fa-check"></i>
									</span>
									<span class="info">{{ $product->upc3 }}</span>
								</a>
							</li>
							<li>
								<a>
									<span class="ico">
                                    <i class="fas fa-check"></i>
									</span>
									<span class="info">{{ $product->upc4 }}</span>
								</a>
							</li>
							
						</ul>
					</div>
				</div>
			</div>
            <div class="tab-container">
					<nav class="nav-wrap" id="sticky-tab">
						<div class="container">
							<!-- nav tabs -->
							<ul class="nav nav-tabs text-center" role="tablist">
								<li role="presentation" class="active"><a href="#tab01" aria-controls="tab01" role="tab" data-toggle="tab">BASIS INFORMATIE</a></li>
								<li role="presentation"><a href="#tab05" aria-controls="tab05" role="tab" data-toggle="tab">VISUELE IMPRESSIE</a></li>
								<li role="presentation"><a href="#tab03" aria-controls="tab03" role="tab" data-toggle="tab">AANVULLENDE GEGEVENS</a></li>
								<!-- <li role="presentation"><a href="#tab04" aria-controls="tab04" role="tab" data-toggle="tab">Faq &amp; Review</a></li>
								<li role="presentation"><a href="#tab05" aria-controls="tab05" role="tab" data-toggle="tab">Gallery</a></li>
								<li role="presentation"><a href="#tab06" aria-controls="tab06" role="tab" data-toggle="tab">Dates &amp; Price</a></li> -->
							</ul>
						</div>
					</nav>
					<!-- tab panes -->
					<div class="container tab-content trip-detail">
						<!-- overview tab content -->
						<div role="tabpanel" class="tab-pane active" id="tab01">
							<div class="row">
								<div class="col-md-12">
									<!-- <strong class="header-box">BASIS INFORMATIE: FORMOSA PARK HOTEL 4*.</strong> -->
                                    <div class="card mb-12" style="margin-top:10px">
                                        <div class="row g-0">
                                            <div class="col-md-4">
                                            <img src="{{ asset('storage/'.$product->images->get(1)->full) }}" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                            <div class="card-body">
                                                <h2 class="card-title" style="color: #33cccc; text-align:center">{{ $product->product_basic_title }}</h2>
                                                <p class="card-text" style="text-align:center;font-size: 1.1em">{{ $product->product_basic_description }}</p>
                                                <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="card mb-12" style="margin-top:10px">
                                        <div class="row g-0">
                                            <div class="col-md-4">
                                            <img src="{{ asset('storage/'.$product->images->get(2)->full) }}" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                            <div class="card-body">
                                                <h2 class="card-title" style="color: #33cccc; text-align:center">{{ $product->product_basic_titlesecond }}</h2>
                                                <p class="card-text" style="text-align:center;font-size: 1.1em">{{ $product->product_basic_descriptionsecond }}</p>
                                                <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="card mb-12" style="margin-top:10px">
                                        <div class="row g-0">
                                            <div class="col-md-4">
                                            <img src="{{ asset('storage/'.$product->images->get(3)->full) }}" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                            <div class="card-body">
                                                <h2 class="card-title" style="color: #33cccc; text-align:center">{{ $product->product_basic_titlethird }}</h2>
                                                <p class="card-text" style="text-align:center;font-size: 1.1em">{{ $product->product_basic_descriptionthird }}</p>
                                                <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="card mb-12" style="margin-top:10px">
                                        <div class="row g-0">
                                            <div class="col-md-4">
                                            <img src="{{ asset('storage/'.$product->images->get(4)->full) }}" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                            <div class="card-body">
                                                <h2 class="card-title" style="color: #33cccc; text-align:center">{{ $product->product_basic_titlefourth }}</h2>
                                                <p class="card-text" style="text-align:center;font-size: 1.1em">{{ $product->product_basic_descriptionfourth }}</p>
                                                <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="card mb-12" style="margin-top:10px">
                                        <div class="row g-0">
                                            <div class="col-md-4">
                                            <img src="{{ asset('storage/'.$product->images->get(5)->full) }}" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                            <div class="card-body">
                                                <h2 class="card-title" style="color: #33cccc; text-align:center">{{ $product->product_basic_titlefifth }}!</h2>
                                                <p class="card-text" style="text-align:center;font-size: 1.1em">{{ $product->product_basic_descriptionfifth }}</p>
                                                <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="card mb-12" style="margin-top:10px">
                                        <div class="row g-0">
                                            <div class="col-md-4">
                                            <img src="{{ asset('storage/'.$product->images->get(6)->full) }}" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                            <div class="card-body">
                                                <h2 class="card-title" style="color: #33cccc; text-align:center">{{ $product->product_basic_titlesix }}</h2>
                                                <p class="card-text" style="text-align:center;font-size: 1.1em">{{ $product->product_basic_descriptionsix }}</p>
                                                <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
                                            </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
								</div>
							</div>
						</div>
						<!-- itinerary tab content -->
						<div role="tabpanel" class="tab-pane" id="tab02">
							<div class="row">
								<div class="col-md-6">
									<ol class="detail-accordion">
										<li>
											<a href="#">
												<strong class="title">Day 1</strong>
												<span>Depart London</span>
											</a>
											<div class="slide">
												<div class="slide-holder">
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ctetur, adipisci velit, sed quia non numquam eius modi.</p>
												</div>
											</div>
										</li>
										<li>
											<a href="#">
												<strong class="title">Day 2</strong>
												<span>Arrive in Kathmandu</span>
											</a>
											<div class="slide">
												<div class="slide-holder">
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ctetur, adipisci velit, sed quia non numquam eius modi.</p>
												</div>
											</div>
										</li>
										<li>
											<a href="#">
												<strong class="title">Day 3</strong>
												<span>Leave for Pokhara</span>
											</a>
											<div class="slide">
												<div class="slide-holder">
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ctetur, adipisci velit, sed quia non numquam eius modi.</p>
												</div>
											</div>
										</li>
										<li>
											<a href="#">
												<strong class="title">Day 4</strong>
												<span>Start Trekking at Besi</span>
											</a>
											<div class="slide">
												<div class="slide-holder">
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ctetur, adipisci velit, sed quia non numquam eius modi.</p>
												</div>
											</div>
										</li>
										<li>
											<a href="#">
												<strong class="title">Day 5</strong>
												<span>Day subtitle message</span>
											</a>
											<div class="slide">
												<div class="slide-holder">
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ctetur, adipisci velit, sed quia non numquam eius modi.</p>
												</div>
											</div>
										</li>
										<li>
											<a href="#">
												<strong class="title">Day 6</strong>
												<span>Day subtitle message</span>
											</a>
											<div class="slide">
												<div class="slide-holder">
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ctetur, adipisci velit, sed quia non numquam eius modi.</p>
												</div>
											</div>
										</li>
										<li>
											<a href="#">
												<strong class="title">Day 7</strong>
												<span>Depart London</span>
											</a>
											<div class="slide">
												<div class="slide-holder">
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ctetur, adipisci velit, sed quia non numquam eius modi.</p>
												</div>
											</div>
										</li>
										<li class="active">
											<a href="#">
												<strong class="title">Day 8</strong>
												<span>Return to London</span>
											</a>
											<div class="slide">
												<div class="slide-holder">
													<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
													<p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. </p>
												</div>
											</div>
										</li>
									</ol>
								</div>
								<div class="col-md-6">
									<article class="img-article article-light">
										<div class="img-wrap">
											<img src="img/generic/img-08.jpg" height="319" width="570" alt="image description">
										</div>
										<div class="text-block">
											<h3><a href="#">Member taking a short break</a></h3>
											<p>Consider packing your bag with folloing daily essentials.</p>
										</div>
									</article>
									<article class="img-article article-light">
										<div class="img-wrap">
											<img src="img/generic/img-09.jpg" height="319" width="570" alt="image description">
										</div>
										<div class="text-block">
											<h3><a href="#">Couple enjoying the spectacular view</a></h3>
											<p>Consider packing your bag with folloing daily essentials.</p>
										</div>
									</article>
								</div>
							</div>
						</div>
						<!-- accomodation tab content -->
						<div role="tabpanel" class="tab-pane" id="tab03">
                            <div class="row">
                                <div class="col-md-12">
										<video width="320" height="240" controls>
											<source src="{{ asset('storage/'.$product->images->get(7)->full) }}" type="video/mp4">
										</video> 

                                        <div class="demo-wrapper">
                                        <div class="row count-block">
                                            <div class="col-xs-6 col-md-3 block-1">
                                                <div class="holder">
                                                    <span style="font-size: 40px;" class="fas fa-golf-ball"></span>
                                                    <span class="info"><span class="counter">{{ $product->digit1 }}</span></span>
                                                    <span class="txt">HOLES GOLFBAAN</span>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-md-3 block-2">
                                                <div class="holder">
                                                    <span style="font-size: 40px;" class="fas fa-bed"></span>
                                                    <span class="info"><span class="counter">{{ $product->digit2 }}</span></span>
                                                    <span class="txt">DELUXE KAMERS</span>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-md-3 block-3">
                                                <div class="holder">
                                                    <span style="font-size: 40px;" class="fas fa-utensils"></span>
                                                    <span class="info"><span class="counter">{{ $product->digit3 }}</span></span>
                                                    <span class="txt">LUXE RESTAURANTS</span>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-md-3 block-4">
                                                <div class="holder">
                                                    <span style="font-size: 40px;" class="fas fa-thumbs-up"></span>
                                                    <span class="info"><span class="counter">5000</span>+</span>
                                                    <span class="txt">TEVREDEN KLANTEN</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<!-- faq and review tab content -->
						<div role="tabpanel" class="tab-pane" id="tab04">
							<div class="row">
								<div class="col-md-6">
									<div class="question-select">
										<select id="tabSelect" class="question">
											<option value="#innerTab1">What kind of footwear is suitable?</option>
											<option value="#innerTab2">What kind of bag is suitable?</option>
											<option value="#innerTab3">What kind of clothes are suitable?</option>
										</select>
										<ul class="nav nav-tabs" id="questionTab">
											<li class="active"><a href="#innerTab1" data-toggle="tab">What kind of footwear wearing is suitable?</a></li>
											<li><a href="#innerTab2" data-toggle="tab">What kind of bag is suitable?</a></li>
											<li><a href="#innerTab3" data-toggle="tab">What kind of clothes wearing is suitable?</a></li>
										</ul>
									</div>
									<div class="tab-wrapper">
										<div role="tabpanel" class="tab-pane active" id="innerTab1">
											<div class="detail">
												<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. </p>
												<p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. </p>
												<p>Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. </p>
												<p>Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. </p>
												<p>Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam.</p>
												<p>Ulins aliquam massa nisl quis neque. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. </p>
												<p>Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam.</p>
												<p>Ulins aliquam massa nisl quis neque. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. </p>
												<p>Suspendisse gin orci enim.</p>
												<ul class="img-list">
													<li>
														<img src="img/generic/img-12.jpg" height="52" width="101" alt="image description">
													</li>
													<li>
														<img src="img/generic/img-13.jpg" height="97" width="114" alt="image description">
													</li>
													<li>
														<img src="img/generic/img-14.jpg" height="104" width="124" alt="image description">
													</li>
												</ul>
												<div class="reviews-slot v-middle">
													<div class="thumb">
														<a href="#"><img src="img/thumbs/img-04.jpg" height="50" width="50" alt="image description"></a>
													</div>
													<div class="text">
														<strong class="name"><a href="#">Jessica Lambert - Customer Relations</a></strong>
													</div>
												</div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane" id="innerTab2">
											<div class="detail">
												<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. </p>
												<p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. </p>
												<p>Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. </p>
												<p>Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. </p>
												<div class="reviews-slot v-middle">
													<div class="thumb">
														<a href="#"><img src="img/thumbs/img-04.jpg" height="50" width="50" alt="image description"></a>
													</div>
													<div class="text">
														<strong class="name"><a href="#">Jessica Lambert - Customer Relations</a></strong>
													</div>
												</div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane" id="innerTab3">
											<div class="detail">
												<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. </p>
												<p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. </p>
												<p>Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. </p>
												<p>Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. </p>
												<p>Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam.</p>
												<p>Ulins aliquam massa nisl quis neque. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. </p>
												<p>Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam.</p>
												<p>Ulins aliquam massa nisl quis neque. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. </p>
												<p>Suspendisse gin orci enim.</p>
												<ul class="img-list">
													<li>
														<img src="img/generic/img-12.jpg" height="52" width="101" alt="image description">
													</li>
													<li>
														<img src="img/generic/img-13.jpg" height="97" width="114" alt="image description">
													</li>
													<li>
														<img src="img/generic/img-14.jpg" height="104" width="124" alt="image description">
													</li>
												</ul>
												<div class="reviews-slot v-middle">
													<div class="thumb">
														<a href="#"><img src="img/thumbs/img-04.jpg" height="50" width="50" alt="image description"></a>
													</div>
													<div class="text">
														<strong class="name"><a href="#">Jessica Lambert - Customer Relations</a></strong>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="header-box">
										<a href="#" class="link-right">Writing A Review</a>
										<span class="rate-left">
											<strong class="title">Overall Rating</strong>
											<span class="star-rating">
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span><span class="icon-star"></span></span>
												<span class="disable"><span class="icon-star"></span></span>
											</span>
											<span class="value">4.7/5</span>
										</span>
									</div>
									<div class="comments reviews-body">
										<div class="comment-holder">
											<div class="comment-slot">
												<div class="thumb">
													<a href="#"><img src="img/thumbs/img-05.jpg" height="50" width="50" alt="image description"></a>
												</div>
												<div class="text">
													<header class="comment-head">
														<div class="left">
															<strong class="name">
																<a href="#">Cleona Torez - Spain</a>
															</strong>
															<div class="meta">Reviewed on 14/1/2016</div>
														</div>
														<div class="right">
															<div class="star-rating">
																<span><span class="icon-star"></span></span>
																<span><span class="icon-star"></span></span>
																<span><span class="icon-star"></span></span>
																<span><span class="icon-star"></span></span>
																<span class="disable"><span class="icon-star"></span></span>
															</div>
															<span class="value">4.7/5</span>
														</div>
													</header>
													<div class="des">
														<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </p>
														<div class="link-holder">
															<a href="#">Read Full Review</a>
														</div>
													</div>
												</div>
											</div>
											<div class="comment-slot">
												<div class="thumb">
													<a href="#"><img src="img/thumbs/img-06.jpg" height="50" width="50" alt="image description"></a>
												</div>
												<div class="text">
													<header class="comment-head">
														<div class="left">
															<strong class="name">
																<a href="#">Cleona Torez - Spain</a>
															</strong>
															<div class="meta">Reviewed on 14/1/2016</div>
														</div>
														<div class="right">
															<div class="star-rating">
																<span><span class="icon-star"></span></span>
																<span><span class="icon-star"></span></span>
																<span><span class="icon-star"></span></span>
																<span><span class="icon-star"></span></span>
																<span class="disable"><span class="icon-star"></span></span>
															</div>
															<span class="value">4.7/5</span>
														</div>
													</header>
													<div class="des">
														<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </p>
														<div class="link-holder">
															<a href="#">Read Full Review</a>
														</div>
													</div>
												</div>
											</div>
											<div class="comment-slot">
												<div class="thumb">
													<a href="#"><img src="img/thumbs/img-07.jpg" height="50" width="50" alt="image description"></a>
												</div>
												<div class="text">
													<header class="comment-head">
														<div class="left">
															<strong class="name">
																<a href="#">Cleona Torez - Spain</a>
															</strong>
															<div class="meta">Reviewed on 14/1/2016</div>
														</div>
														<div class="right">
															<div class="star-rating">
																<span><span class="icon-star"></span></span>
																<span><span class="icon-star"></span></span>
																<span><span class="icon-star"></span></span>
																<span><span class="icon-star"></span></span>
																<span class="disable"><span class="icon-star"></span></span>
															</div>
															<span class="value">4.7/5</span>
														</div>
													</header>
													<div class="des">
														<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </p>
														<div class="link-holder">
															<a href="#">Read Full Review</a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="link-more text-center">
											<a href="#">Show More Reviews - 75 Reviews</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- gallery tab content -->
						<div role="tabpanel" class="tab-pane" id="tab05">
							<ul class="row gallery-list has-center">
							<?php $count = 0;?>
							@if ($product->images->count() > 0)
							@foreach($product->images as $image)
							@if($count>7)
								<li class="col-sm-6">
									<a class="fancybox" data-fancybox-group="group" href="{{ asset('storage/'.$image->full) }}" title="Caption Goes Here">
										<span class="img-holder">
											<img src="{{ asset('storage/'.$image->full) }}" height="240" width="370" alt="image description">
										</span>
										<span class="caption">
											<span class="centered">
											<span><span class="icon-image"></span></span>
											</span>
										</span>
									</a>
								</li>
								@endif
    						<?php $count++ ?>
							@endforeach
							@else
							<li class="col-sm-6">
									<a class="fancybox" data-fancybox-group="group" href="https://www.golfreizen.store/wp-content/uploads/2020/08/Golfreizen-Formosa-Park-Hotel-1.jpg" title="Caption Goes Here">
										<span class="img-holder">
											<img src="https://www.golfreizen.store/wp-content/uploads/2020/08/Golfreizen-Formosa-Park-Hotel-1.jpg" height="240" width="370" alt="image description">
										</span>
										<!-- <span class="caption">
											<span class="centered">
												<strong class="title">ANNAPURNA VIEW</strong>
												<span class="sub-text">The Classic Trek</span>
											</span>
										</span> -->
									</a>
							</li>
							@endif
							</ul>
						</div>
						<!-- dates and prices tab content -->
						<div role="tabpanel" class="tab-pane" id="tab06">
							<div class="table-container">
								<div class="table-responsive">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>
													<strong class="date-text">Departure Dates</strong>
													<span class="sub-text">Confirmed Dates</span>
												</th>
												<th>
													<strong class="date-text">Trip Status</strong>
													<span class="sub-text">Trip Status</span>
												</th>
												<th>
													<strong class="date-text">Price (PP)</strong>
													<span class="sub-text">Including Flights</span>
												</th>
												<th>
													<strong class="date-text">Price (PP)</strong>
													<span class="sub-text">Excluding Flights</span>
												</th>
												<th>
													&nbsp;
												</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><div class="cell"><div class="middle">Fri 18 Oct '16 - Sun 04 Nov '16</div></div></td>
												<td><div class="cell"><div class="middle">Available &amp; Guaranteed</div></div></td>
												<td><div class="cell"><div class="middle">$2,779</div></div></td>
												<td><div class="cell"><div class="middle">$3,170</div></div></td>
												<td><div class="cell"><div class="middle">
													<a href="#" class="btn btn-default">BOOK NOW</a>
												</div></div></td>
											</tr>
											<tr>
												<td><div class="cell"><div class="middle">Fri 18 Oct '16 - Sun 04 Nov '16</div></div></td>
												<td><div class="cell"><div class="middle">Booked &amp; Guaranteed</div></div></td>
												<td><div class="cell"><div class="middle">$2,679</div></div></td>
												<td><div class="cell"><div class="middle">$3,970</div></div></td>
												<td><div class="cell"><div class="middle">
													<a href="#" class="btn btn-default">BOOK NOW</a>
												</div></div></td>
											</tr>
											<tr>
												<td><div class="cell"><div class="middle">Fri 18 Oct '16 - Sun 04 Nov '16</div></div></td>
												<td><div class="cell"><div class="middle">Available &amp; Guaranteed</div></div></td>
												<td><div class="cell"><div class="middle">$1,779</div></div></td>
												<td><div class="cell"><div class="middle">$3,470</div></div></td>
												<td><div class="cell"><div class="middle">
													<a href="#" class="btn btn-default">BOOK NOW</a>
												</div></div></td>
											</tr>
											<tr>
												<td><div class="cell"><div class="middle">Fri 18 Oct '16 - Sun 04 Nov '16</div></div></td>
												<td><div class="cell"><div class="middle">Available</div></div></td>
												<td><div class="cell"><div class="middle">$2,779</div></div></td>
												<td><div class="cell"><div class="middle">$3,970</div></div></td>
												<td><div class="cell"><div class="middle">
													<a href="#" class="btn btn-default">BOOK NOW</a>
												</div></div></td>
											</tr>
											<tr>
												<td><div class="cell"><div class="middle">Fri 18 Oct '16 - Sun 04 Nov '16</div></div></td>
												<td><div class="cell"><div class="middle">Booked &amp; Guaranteed</div></div></td>
												<td><div class="cell"><div class="middle">$3,779</div></div></td>
												<td><div class="cell"><div class="middle">$4,970</div></div></td>
												<td><div class="cell"><div class="middle">
													<a href="#" class="btn btn-default">BOOK NOW</a>
												</div></div></td>
											</tr>
											<tr>
												<td><div class="cell"><div class="middle">Fri 18 Oct '16 - Sun 04 Nov '16</div></div></td>
												<td><div class="cell"><div class="middle">Available &amp; Guaranteed</div></div></td>
												<td><div class="cell"><div class="middle">$2,879</div></div></td>
												<td><div class="cell"><div class="middle">$3,970</div></div></td>
												<td><div class="cell"><div class="middle">
													<a href="#" class="btn btn-default">BOOK NOW</a>
												</div></div></td>
											</tr>
											<tr>
												<td><div class="cell"><div class="middle">Fri 18 Oct '16 - Sun 04 Nov '16</div></div></td>
												<td><div class="cell"><div class="middle">Available &amp; Guaranteed</div></div></td>
												<td><div class="cell"><div class="middle">$2,679</div></div></td>
												<td><div class="cell"><div class="middle">$1,970</div></div></td>
												<td><div class="cell"><div class="middle">
													<a href="#" class="btn btn-default">BOOK NOW</a>
												</div></div></td>
											</tr>
											<tr>
												<td><div class="cell"><div class="middle">Fri 18 Oct '16 - Sun 04 Nov '16</div></div></td>
												<td><div class="cell"><div class="middle">Booked &amp; Guaranteed</div></div></td>
												<td><div class="cell"><div class="middle">$4,779</div></div></td>
												<td><div class="cell"><div class="middle">$3,970</div></div></td>
												<td><div class="cell"><div class="middle">
													<a href="#" class="btn btn-default">BOOK NOW</a>
												</div></div></td>
											</tr>
											<tr>
												<td><div class="cell"><div class="middle">Fri 18 Oct '16 - Sun 04 Nov '16</div></div></td>
												<td><div class="cell"><div class="middle">Available &amp; Guaranteed</div></div></td>
												<td><div class="cell"><div class="middle">$5,779</div></div></td>
												<td><div class="cell"><div class="middle">$3,270</div></div></td>
												<td><div class="cell"><div class="middle">
													<a href="#" class="btn btn-default">BOOK NOW</a>
												</div></div></td>
											</tr>
											<tr>
												<td><div class="cell"><div class="middle">Fri 18 Oct '16 - Sun 04 Nov '16</div></div></td>
												<td><div class="cell"><div class="middle">Available &amp; Guaranteed</div></div></td>
												<td><div class="cell"><div class="middle">$2,779</div></div></td>
												<td><div class="cell"><div class="middle">$3,970</div></div></td>
												<td><div class="cell"><div class="middle">
													<a href="#" class="btn btn-default">BOOK NOW</a>
												</div></div></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="load-more text-center text-uppercase">
									<a href="#">MORE DATES &amp; PRICES</a>
								</div>
							</div>
						</div>
					</div>
				</div>
    
@stop
@push('scripts')
    <script>
        $(document).ready(function () {
            $('#addToCart').submit(function (e) {
                if ($('.option').val() == 0) {
                    e.preventDefault();
                    alert('Please select an option');
                }
            });
            $('.option').change(function () {
                $('#productPrice').html("{{ $product->sale_price != '' ? $product->sale_price : $product->price }}");
                let extraPrice = $(this).find(':selected').data('price');
                let price = parseFloat($('#productPrice').html());
                let finalPrice = (Number(extraPrice) + price).toFixed(2);
                $('#finalPrice').val(finalPrice);
                $('#productPrice').html(finalPrice);
            });
        });
    </script>
@endpush
