@extends('site.app')
@section('title', 'Voetbalreizen')
@section('content')

<section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" id="gridview-sidebar">
				<div class="banner-text">
					<div class="center-text">
						<div class="container">
							<h1>ONS VOETBALREIZEN AANBOD:</h1>
							<strong class="subtitle">Hier ziet u ons club overzicht van de 24 beste clubs wereldwijd!</strong>
							<!-- breadcrumb -->
							<nav class="breadcrumbs">
								<ul>
									<li><a href="#">HOME</a></li>
									<li><a>Voetbalreizen</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
</section>
<main id="main">
		
        <div class="container tab-content trip-detail">

            <!-- gallery tab content -->
                <ul class="row gallery-list has-center">
                    @foreach ($categories as $category)
                    <li class="col-sm-6">
                        <a class="fancybox" data-fancybox-group="group" href="/voetbalreizen/{{$category->slug}}" title="Caption Goes Here">
                            <span class="img-holder">
                                <img src="{{ url('storage/category/'.$category->mainimage) }}" height="240" width="370" alt="image description">
                            </span>
                            <span class="caption">
                                <span class="centered">
                                    <strong class="title"> {{ str_replace('Voetbalreizen |', '', $category->name) }}</strong>
                                    <span class="sub-text">Bezoek het altijd geweldige Old Trafford!!</span>
                                </span>
                            </span>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>

    </aside>
</main>

@stop
