@extends('site.app')
@section('title', 'Voetbalreizen')
@section('content')

<section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" id="gridview-sidebar">
				<div class="banner-text">
					<div class="center-text">
						<div class="container">
							<h1>ONS GOLFREIZEN AANBOD:</h1>
							<strong class="subtitle">Hier ziet u ons resort overzicht van de 100 mooiste
                                verblijven!</strong>
							<!-- breadcrumb -->
							<nav class="breadcrumbs">
								<ul>
									<li><a href="#">HOME</a></li>
									<li><a>Golfreizen</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
</section>
<main id="main">
<div class="featured-content adventure-holder">
                    <div class="container-fluid">
                    @foreach ($categories as $category)
                        <div class="row same-height">
                            <div class="col-md-6 image height wow slideInLeft">
                                <div class="bg-stretch">
                                    <img src="{{ url('storage/category/'.$category->mainimage) }}" height="627" width="960"
                                        alt="image description">
                                </div>
                            </div>
                            <div class="col-md-6 text-block height wow slideInRight">
                                <div class="centered">
                                    <h2 class="intro-heading">{{ str_replace('Voetbalreizen |', '', $category->name) }}</h2>
                                    <p class="intro">{{ $category->description }}</p>
                                    <a href="/golfreizen/{{$category->slug}}" class="btn btn-primary btn-lg">GENIET!</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
</div>

</main>

@stop
