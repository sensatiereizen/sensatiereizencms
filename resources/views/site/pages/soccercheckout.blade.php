@extends('site.app')
@section('title', 'Checkout')
@section('content')
<section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" id="gridview-sidebar">
            <div class="banner-text">
					<div class="center-text">
						<div class="container">
							<h1>Checkout Voetbalreizen</h1>
							<!-- breadcrumb -->
							<nav class="breadcrumbs">
								<ul>
									<li><a href="#">HOME</a></li>
									<li><a>Checkout</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
</section>
   

	<main id="main">

				<div class="inner-main common-spacing container">
					<!-- booking form -->
					<div class="row">
                <div class="col-sm-12">
                    @if (Session::has('error'))
                        <p class="alert alert-danger">{{ Session::get('error') }}</p>
                    @endif
                </div>
            </div>
					<form class="booking-form" action="{{ route('checkout.place.order') }}" method="POST" role="form">
						<div class="row">
							<div class="col-md-6">
							<div class="form-holder">
									<h2 class="small-size">Billing Information</h2>
									<div class="wrap">
										<div class="row">
											<div class="col-md-6">
												<div class="hold">
													<label for="name">First Name</label>
													<input type="text" id="first_name" name="first_name" class="form-control">
												</div>
											</div>
											<div class="col-md-6">
												<div class="hold">
													<label for="lname">Last Name</label>
													<input type="text" id="last_name" name="last_name" class="form-control">
												</div>
											</div>
										</div>
										<!-- <div class="hold">
											<label for="cname">Company Name</label>
											<input type="text" id="cname" class="form-control">
										</div> -->
										<div class="hold">
											<label for="address">Address</label>
											<input type="text" id="address" name="address" class="form-control">
										</div>
										<div class="hold">
											<label for="city">City</label>
											<input type="text" id="city" name="city" name="city" class="form-control">
										</div>
                                        <div class="hold">
											<label for="city">Country</label>
											<input type="text" id="country" name="country" class="form-control">
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="hold">
													<label for="em">Email address</label>
													<input type="email" id="email" name="email" class="form-control">
												</div>
											</div>
											<div class="col-md-6">
												<div class="hold">
													<label for="phn">Phone</label>
													<input type="text" id="phone_number" name="phone_number" class="form-control">
												</div>
											</div>
										</div>
										<!-- <ul class="option">
											<li>
												<header class="title">
													<label class="custom-radio">
														<input type="radio" name="pay">
														<span class="check-input"></span>
														<span class="check-label">Check Payment</span>
													</label>
												</header>
												<div class="info-hold">
													<p>Please send your cheque to Entrada Inc. Address, Thank You!</p>
												</div>
											</li>
											<li>
												<header class="title">
													<label class="custom-radio">
														<input type="radio" name="pay">
														<span class="check-input"></span>
														<span class="check-label">Paypal Payment</span>
													</label>
												</header>
												<div class="info-hold">
													<p>If you dont have paypal account - you can still pay using credit cards!</p>
												</div>
											</li>
										</ul> -->
										<!-- <ul class="payment-option">
											<li>
												<img alt="visa" src="img/footer/visa.png">
											</li>
											<li>
												<img width="33" height="20" alt="master card" src="img/footer/mastercard.png">
											</li>
											<li>
												<img width="72" height="20" alt="paypal" src="img/footer/paypal.png">
											</li>
											<li>
												<img width="33" height="20" alt="maestro" src="img/footer/maestro.png">
											</li>
											<li>
												<img width="55" height="20" alt="bank transfer" src="img/footer/bank-transfer.png">
											</li>
										</ul> -->
									</div>
								</div>
								<div class="form-holder">
									<h2 class="small-size">Uw Basisarrangement</h2>
									<div class="wrap">
										<div class="row">
											<div class="col-md-6">
												<div class="hold">
													<label for="name">Naam accomodatie</label>
													<input type="text" id="acom" name="acom" class="form-control">
												</div>
											</div>
											<div class="col-md-6">
												<div class="hold">
													<label for="lname">Gewenste aankomstdatum</label>
													<input type="text" id="date" name="last_name" class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="hold">
													<label for="name">Gewenste vertrekdatum</label>
													<input type="text" id="acom" name="acom" class="form-control">
												</div>
											</div>
											<div class="col-md-6">
												<div class="hold">
													<label for="lname">Aantal personen</label>
													<input type="number" id="date" name="last_name" class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="hold">
													<label for="name">Kamerindeling</label>
													<input type="text" id="acom" name="acom" class="form-control">
												</div>
											</div>
											<div class="col-md-6">
												<div class="hold">
													<label for="lname">Vlucht</label>
													<input type="number" id="date" name="flight" class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="hold">
													<label for="name">Voorkeur vlucht vanaf: Meerdere mogelijkheden</label></br>
													<select class="form-select" id="acom" name="acom"aria-label="Default select example">
														<option selected>Kies een optie</option>
														<option value="1">One</option>
														<option value="2">Two</option>
														<option value="3">Three</option>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-holder">
									<h2 class="small-size">Additional Notes</h2>
									<div class="wrap">
										<div class="hold">
											<label for="txt">Your Comment</label>
											<textarea id="txt" class="form-control" name="notes" placeholder="Please enter any additional information here, eg. food/drug requirements etc."></textarea>
										</div>
									</div>
									<div class="order-block">
										<h2 class="small-size">Preview Order</h2>
										<div class="wrap">
											<table class="product-table">
												<thead>
													<tr>
														<th>Resort</th>
														<th>Totaal Prijs</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															<span class="title">Everest Trekking &emsp;<span class="amount">x&emsp; 3</span></span>
															<time datetime="2016-09-29">Booking Date:&emsp; 14th Jan 2016</time>
														</td>
														<td>
															<span class="amount">$2,999</span>
														</td>
													</tr>
													<tr>
														<td>
															<span class="title">Bungee Jumping &emsp;<span class="amount">x&emsp; 2</span></span>
															<time datetime="2016-09-29">Booking Date:&emsp; 14th Jan 2016</time>
														</td>
														<td>
															<span class="amount">$1,999</span>
														</td>
													</tr>
												</tbody>
												<tfoot>
													<tr>
														<td>Totaal</td>
														<td>{{ config('settings.currency_symbol') }}{{ \Cart::getSubTotal() }}</td>
													</tr>
												</tfoot>
											</table>
											<button type="submit" class="btn btn-default">Verzenden</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</main>

@stop
