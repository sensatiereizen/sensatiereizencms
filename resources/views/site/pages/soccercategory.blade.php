@extends('site.app')
@section('title', $category->name)
@section('content')
<section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" id="gridview-sidebar" style="background-image: url('{{ url('storage/'.$category->image) }}')">
				<div class="banner-text">
					<div class="center-text">
						<div class="container">
							<h1>{{ $category->name }}</h1>
							<strong class="subtitle">{{ $category->description }}</strong>
							<!-- breadcrumb -->
							<nav class="breadcrumbs">
								<ul>
									<li><a href="#">HOME</a></li>
									<li><a>{{ $category->name }}</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
</section>
<main id="main">
				<!-- content with sidebar -->
				<div class="bg-gray content-with-sidebar grid-view-sidebar">
					<div class="container">
						<div id="two-columns" class="row">
							<!-- sidebar -->
							<!-- <aside id="sidebar" class="col-md-4 col-lg-3 sidebar sidebar-list">
								<div class="sidebar-holder">
									<header class="heading">
										<h3>FILTER</h3>
									</header>
								</div>
							</aside> -->
							<div id="content" class="col-md-8 col-lg-12">
								<div class="filter-option filter-box">
									<strong class="result-info">{{count($category->products)}} VOETBALREIZEN</strong>
									<!-- <div class="layout-holder">
										<div class="layout-action">
											<a href="#" class="link link-list"><span class="icon-list"></span></a>
											<a href="#" class="link link-grid active"><span class="icon-grid"></span></a>
										</div>
										<div class="select-holder">
											<div class="select-col">
												<select class="filter-select sort-select">
													<option value="sort">Sort Order</option>
													<option value="sort">Price</option>
													<option value="sort">Recent</option>
													<option value="sort">Popular</option>
												</select>
											</div>
										</div>
									</div> -->
								</div>
								<div class="content-holder content-sub-holder">
									<div class="row db-3-col">
                                    @forelse($category->products as $product)
		
										<article class="col-md-6 col-lg-3 article has-hover-s1 thumb-full">
                                            <div class="thumbnail">
                                            @if ($product->images->count() > 0)
							
										

                                                <div class="img-wrap">
                                                    <img src="{{ url('storage/'.$product->images->first()->full) }}" height="228" width="350" alt="">
                                                </div>
                                            @else
                                                <div class="img-wrap">
                                                    <img src="{{ asset('frontend/images/listing/img-19.jpg') }}" height="228" width="350" alt="">
                                                </div>
                                            @endif
                                                <h3 class="small-space"><a href="{{ route('product.show', $product->slug) }}">{{ $product->name }}</a></h3>
                                                <aside class="meta">
                                                </aside>
                                                <p style="margin-botom:0px;">{{ $product->description }}</p>
                                                <p>{{ $product->upc1 }}</p>
                                                <p>{{ $product->upc2 }}</p>
                                                <!-- <a href="{{ route('product.show', $product->slug) }}" class="btn btn-default">MEER INFO</a> -->
												<form action="{{ route('product.socceradd.cart') }}" method="POST" role="form" id="addToCart">
												@csrf

													<dt style="visibility:hidden;">Quantity: </dt>
																<dd style="visibility:hidden;">
																	<input class="form-control" type="number" min="1" value="1" max="{{ $product->quantity }}" name="qty" style="width:70px;">
																	<input type="hidden" name="productId" value="{{ $product->id }}">
																	<input type="hidden" name="price" id="finalPrice" value="{{ $product->sale_price != '' ? $product->sale_price : $product->price }}">
													</dd>
													<button type="submit" class="btn btn-default">OFFERTE</button>
										
												</form>
												<!-- <form action="{{ route('product.socceradd.cart') }}" method="POST" role="form" id="addToCart">
												@csrf
												<dd style="visibility:hidden;">
                                                        <input class="form-control" type="number" min="1" value="1" max="{{ $product->quantity }}" name="qty" style="width:70px;">
                                                        <input type="hidden" name="productId" value="{{ $product->id }}">
                                                        <input type="hidden" name="price" id="finalPrice" value="{{ $product->sale_price != '' ? $product->sale_price : $product->price }}">
                                            	</dd>
												<button type="submit" class="btn btn-default">OFFERTE</button>
												</form> -->
                                                <footer>
                                                    @if ($product->sale_price != 0)
                                                        <span class="price">vanaf <span>{{ number_format($product->sale_price, 2, ',', '.') }}</span></span>
                                                        <span class="price-old">vanaf <span>€ {{ number_format($product->price, 2, ',', '.') }}</span></span>
                                                    @else
                                                        <span class="price">vanaf <span> € {{ number_format($product->price, 0, ',', '.') }} </span></span>
                                                    @endif
                                                </footer>
                                            </div>
                                        </article>
                           
                                    @empty
                                        <p>Momenteel zijn er geen voetbalreizen beschikbaar in {{ $category->name }}.</p>
                                    @endforelse
									</div>
								</div>
								<!-- pagination wrap -->
								<!-- <nav class="pagination-wrap">
									<div class="btn-prev">
										<a href="#" aria-label="Previous">
											<span class="icon-angle-right"></span>
										</a>
									</div>
									<ul class="pagination">
										<li><a href="#">1</a></li>
										<li><a href="#">2</a></li>
										<li><a href="#">3</a></li>
										<li class="active"><a href="#">4</a></li>
										<li><a href="#">5</a></li>
										<li>...</li>
										<li><a href="#">7</a></li>
									</ul>
									<div class="btn-next">
										<a href="#" aria-label="Previous">
											<span class="icon-angle-right"></span>
										</a>
									</div>
								</nav> -->
							</div>
						</div>
					</div>
				</div>
			</main>
@stop
