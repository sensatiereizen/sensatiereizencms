@extends('site.app')
@section('title', $category->name)
@section('content')
<section class="banner banner-inner parallax" data-stellar-background-ratio="0.5" id="gridview-sidebar" style="background-image: url('{{ url('storage/'.$category->image) }}')">
				<div class="banner-text">
					<div class="center-text">
						<div class="container">
							<h1>{{ $category->name }}</h1>
							<strong class="subtitle">{{ $category->description }}</strong>
							<!-- breadcrumb -->
							<nav class="breadcrumbs">
								<ul>
									<li><a href="#">HOME</a></li>
									<li><a>{{ $category->name }}</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
</section>
<main id="main">
				<!-- content with sidebar -->
				<div class="bg-gray content-with-sidebar grid-view-sidebar">
					<div class="container">
						<div id="two-columns" class="row">
							<!-- sidebar -->
							<aside id="sidebar" class="col-md-4 col-lg-3 sidebar sidebar-list">
								<div class="sidebar-holder">
									<header class="heading">
										<h3>FILTER</h3>
									</header>
									@foreach ($brands as $brand)
										<label class="m-checkbox">
											<input
												name="brand" type="checkbox" id="filter" value="{{ $brand->id }}"
												@if (in_array($brand->id, explode(',', request()->input('filter.brand'))))
													checked
												@endif
											>
											{{ $brand->name }}
										</label>
									@endforeach
								
								</div>
							</aside>
							<div id="content" class="col-md-8 col-lg-9">
								<div class="filter-option filter-box">
									<strong class="result-info">{{count($category->products)}} GOLFRESORTS</strong>
									<div class="layout-holder">
										<div class="layout-action">
											<a href="#" class="link link-list"><span class="icon-list"></span></a>
											<a href="#" class="link link-grid active"><span class="icon-grid"></span></a>
										</div>
										<div class="select-holder">
											<div class="select-col">
												<select class="filter-select sort-select">
													<option value="sort">Sort Order</option>
													<option value="sort">Price</option>
													<option value="sort">Recent</option>
													<option value="sort">Popular</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								@if ($category->products->count() > 0)
								<div class="content-holder content-sub-holder">
									<div class="row db-3-col">
                                    @forelse($category->products as $product)
                                    <a href="{{ route('product.show', $product->slug) }}">
										<article class="col-md-6 col-lg-4 article has-hover-s1 thumb-full">
                                            <div class="thumbnail">
                                            @if ($product->images->count() > 0)
							
										

                                                <div class="img-wrap">
                                                    <img src="{{ url('storage/'.$product->images->skip(1)->first()->full) }}" height="228" width="350" alt="">
                                                </div>
                                            @else
                                                <div class="img-wrap">
                                                    <img src="{{ asset('frontend/images/listing/img-19.jpg') }}" height="228" width="350" alt="">
                                                </div>
                                            @endif
                                                <h3 class="small-space"><a href="{{ route('product.show', $product->slug) }}">{{ $product->name }}</a></h3>
                                                <aside class="meta">
                                                </aside>
                                                <p>{{ $product->description }}</p>
                                                <a href="{{ route('product.show', $product->slug) }}" class="btn btn-default">MEER INFO</a>
                                                <footer>
                                                    @if ($product->sale_price != 0)
                                                        <span class="price">vanaf <span>{{ config('settings.currency_symbol').$product->sale_price }}</span></span>
                                                        <span class="price-old">vanaf <span></span></span>
                                                    @else
                                                        <span class="price">vanaf <span>{{ number_format($product->price, 2, ',', '.') }}</span></span>
                                                    @endif
                                                </footer>
                                            </div>
                                        </article>
                                    </a>
                                    @empty
                                        <p>No Products found in {{ $category->name }}.</p>
                                    @endforelse
									</div>
								</div>
								@endif
								<!-- pagination wrap -->
								<!-- <nav class="pagination-wrap">
									<div class="btn-prev">
										<a href="#" aria-label="Previous">
											<span class="icon-angle-right"></span>
										</a>
									</div>
									<ul class="pagination">
										<li><a href="#">1</a></li>
										<li><a href="#">2</a></li>
										<li><a href="#">3</a></li>
										<li class="active"><a href="#">4</a></li>
										<li><a href="#">5</a></li>
										<li>...</li>
										<li><a href="#">7</a></li>
									</ul>
									<div class="btn-next">
										<a href="#" aria-label="Previous">
											<span class="icon-angle-right"></span>
										</a>
									</div>
								</nav> -->
							</div>
						</div>
					</div>
				</div>
			</main>

			<script>
    function getIds(checkboxName) {
        let checkBoxes = document.getElementsByName(checkboxName);
        let ids = Array.prototype.slice.call(checkBoxes)
                        .filter(ch => ch.checked==true)
                        .map(ch => ch.value);
        return ids;
    }

    function filterResults () {
        let brandIds = getIds("brand");
		console.log(brandIds)
        let catagoryIds = getIds("catagory");

        let href = 'portugal?';

        if(brandIds.length) {
            href += 'filter[brand]=' + brandIds;
        }

        if(catagoryIds.length) {
            href += '&filter[category]=' + catagoryIds;
        }

        document.location.href=href;
    }

    document.getElementById("filter").addEventListener("change", filterResults);
</script>
@stop
