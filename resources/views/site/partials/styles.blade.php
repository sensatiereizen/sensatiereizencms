<link rel="shortcut icon" type="image/x-icon" href="{{ asset('frontend/images/favicon.ico') }}">

<link href="{{ asset('frontend/fonts/fontawesome/css/fontawesome.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('frontend/css/fonts/icomoon/icomoon.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('frontend/vendors/animate/animate.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('frontend/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ asset('frontend/vendors/owl-carousel/owl.carousel.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('frontend/vendors/owl-carousel/owl.theme.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('frontend/vendors/revolution/css/settings.css') }}" type="text/css" rel="stylesheet">

<link media="all" rel="stylesheet" href="{{ asset('frontend/vendors/fancybox/jquery.fancybox.css') }}">

<link href="{{ asset('frontend/css/main.css') }}" rel="stylesheet" type="text/css" />
<script src="https://kit.fontawesome.com/7993fc6728.js" crossorigin="anonymous"></script>

<!-- <link href="{{ asset('frontend/plugins/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
<link href="{{ asset('frontend/plugins/owlcarousel/assets/owl.theme.default.css') }}" rel="stylesheet"> -->
<!-- <link href="{{ asset('frontend/css/ui.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('frontend/css/responsive.css') }}" rel="stylesheet" media="only screen and (max-width: 1200px)" /> -->
