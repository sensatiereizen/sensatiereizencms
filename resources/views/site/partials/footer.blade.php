		<footer id="footer" style="background: #252525 url({{ asset('frontend/images/footer/footer-pattern.png') }});">
			<div class="container">
				<!-- newsletter form -->
				<form action="php/subscribe.html" id="signup" method="post" class="newsletter-form">
					<fieldset>
						<div class="input-holder">
							<input type="email" class="form-control" placeholder="E-mail Adres" name="subscriber_email" id="subscriber_email">
							<input type="submit" value="Let's GO!">
						</div>
						<span class="info" id="subscribe_message">Voor het direct ontvangen van nieuws, updates en nieuwe golfpakketten in uw inbox.</span>
					</fieldset>
				</form>
				<!-- footer links -->
				<div class="row footer-holder">
					<nav class="col-sm-4 col-lg-3 footer-nav active">
						<h3>Bestemmingen</h3>
						<ul class="slide">
							<li><a href="#">Portugal</a></li>
							<li><a href="#">Spanje</a></li>
							<li><a href="#">Italië</a></li>
							<li><a href="#">Frankrijk</a></li>
							<li><a href="#">Canarische Eilanden</a></li>
						</ul>
					</nav>
					<nav class="col-sm-4 col-lg-3 footer-nav">
						<h3>Meest Geboekt</h3>
						<ul class="slide">
							<li><a href="#">Castelfalfi Golf Resort 5*</a></li>
							<li><a href="#">Terre Blanche Spa & Golf 5*</a></li>
							<li><a href="#">PGA Catalunya Resort 5*</a></li>
							<li><a href="#">Salobre Serenity Resort 5*</a></li>
							<li><a href="#">Verdura Golf & Spa Resort 5*</a></li>
						</ul>
					</nav>
					<nav class="col-sm-4 col-lg-3 footer-nav">
						<h3>Daarom SR</h3>
						<ul class="slide">
							<li><a href="#">Huurauto regelen</a></li>
							<li><a href="#">Unieke Golfclinics</a></li>
							<li><a href="#">Golfset huren</a></li>
							<li><a href="#">Verzeker uzelf</a></li>
							<li><a href="#">Aanvullenden opties</a></li>
						</ul>
					</nav>
					<nav class="col-sm-4 col-lg-3 footer-nav last">
						<h3>Contactgegevens</h3>
						<ul class="slide address-block">
							<li class="wrap-text"><span class="icon-tel"></span> <a href="tel:+31 (0)6 53 69 51 83">+31 (0)6 53 69 51 83</a></li>
							<li class="wrap-text"><span class="icon-tel"></span> <a href="tel:+31 (0)6 51 59 70 03"> +31 (0)6 51 59 70 03</a></li>
							<li class="wrap-text"><span class="icon-email"></span> <a href="mailto: info@sensatiereizen.nl"> info@sensatiereizen.nl</a></li>
							<li><span class="icon-home"></span> <address> Adriaen Aertszoonstraat 14, </br>4931 RZ te Geertruidenberg</address></li>
						</ul>
					</nav>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-lg-7">
							<!-- copyright -->
							<strong class="copyright"><i class="fa fa-copyright"></i> 2010 – 2021 Sensatie Reizen – Golfsensatie | Onderdeel van de Sensation Travel Group</strong>
						</div>
						<div class="col-lg-5">
							<ul class="payment-option">
								<li>
									<img src="{{ asset('frontend/images/footer/IDEAL.png') }}" alt="ideal">
								</li>
								<li>
									<img src="{{ asset('frontend/images/footer/paypal.png') }}" alt="paypal">
								</li>
								<li>
									<img src="{{ asset('frontend/images/footer/visa.png') }}" alt="visa">
								</li>
								<li>
									<img src="{{ asset('frontend/images/footer/mastercard.png') }}" height="20" width="33" alt="master card">
								</li>
								<li>
									<img src="{{ asset('frontend/images/footer/maestro.png') }}" height="20" width="33" alt="maestro">
								</li>
							</ul>
						</div>
					</div>
	</div>
		</footer>