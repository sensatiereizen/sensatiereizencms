<nav class="navbar navbar-default">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle nav-opener" data-toggle="collapse" data-target="#nav">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<!-- main menu items and drop for mobile -->
						<div class="collapse navbar-collapse" id="nav">
							<!-- main navbar -->
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="/">Home <b class="icon-angle-down"></b></a>
								</li>
								<li class="dropdown has-mega-dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Vind je trip <b class="icon-angle-down"></b></a>
									<div class="dropdown-menu">
										<div class="drop-wrap">
											<div class="drop-holder">
												<div class="row">
													<div class="col-sm-5 col-md-4">
														<div class="col">
															<div class="img-wrap">
																<a href="{{ route('category.show', 2) }}"><img src="{{ asset('frontend/images/menu/Header-Menu-Golfreizen-Portugal.png') }}" height="228" width="350" alt="image description"></a>
															</div>
															<div class="des">
																<strong class="title"><a href="hiking-camping.html">Portugal</a></strong>
															</div>
														</div>
													</div>
													<div class="col-sm-5 col-md-4">
														<div class="col">
															<div class="img-wrap">
																<a href="jungle-safari.html"><img src="{{ asset('frontend/images/menu/Header-Menu-Golfreizen-Frankrijk.png') }}" height="215" width="370" alt="image description"></a>
															</div>
															<div class="des">
																<strong class="title"><a href="jungle-safari.html">Frankrijk</a></strong>
															</div>
														</div>
													</div>
													<div class="col-sm-5 col-md-4">
														<div class="col">
															<div class="img-wrap">
																<a href="city-tour.html"><img src="{{ asset('frontend/images/menu/Header-Menu-Golfreizen-Spanje.png') }}" height="215" width="370" alt="image description"></a>
															</div>
															<div class="des">
																<strong class="title"><a href="city-tour.html">Spanje</a></strong>
															</div>
														</div>
													</div>
													<div class="col-sm-5 col-md-4">
														<div class="col">
															<div class="img-wrap">
																<a href="family-fun.html"><img src="{{ asset('frontend/images/menu/Header-Menu-Golfreizen-Italië.png') }}" height="215" width="370" alt="image description"></a>
															</div>
															<div class="des">
																<strong class="title"><a href="family-fun.html">Italië</a></strong>
															</div>
														</div>
													</div>
													<div class="col-sm-5 col-md-4">
														<div class="col">
															<div class="img-wrap">
																<a href="family-fun.html"><img src="{{ asset('frontend/images/menu/Header-Menu-Golfreizen-Canarische-Eilanden.png') }}" height="215" width="370" alt="image description"></a>
															</div>
															<div class="des">
																<strong class="title"><a href="family-fun.html">Canarische Eilanden</a></strong>
															</div>
														</div>
													</div>
													<div class="col-sm-5 col-md-4">
														<div class="col">
															<div class="img-wrap">
																<a href="family-fun.html"><img src="{{ asset('frontend/images/menu/Header-Menu-Golfreizen-Allen.png') }}" height="215" width="370" alt="image description"></a>
															</div>
															<div class="des">
																<strong class="title"><a href="family-fun.html">Alle Landen</a></strong>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Daarom SR <b class="icon-angle-down"></b></a>
                                    <div class="dropdown-menu">
                                        <ul>
                                            <li><a href="grid-view-2-column.html">Huurauto regelen</a></li>
                                            <li><a href="grid-view-2-column.html">Unieke Golfclinics</a></li>
                                            <li><a href="grid-view-2-column.html">Golfset huren</a></li>
                                            <li><a href="grid-view-2-column.html">Verzeker uzelf</a></li>
                                            <li><a href="grid-view-2-column.html">Park & Fly Optie</a></li>
                                            <li><a href="grid-view-2-column.html">Bericht thuisfront</a></li>
                                            <li><a href="grid-view-2-column.html">Sensatie Disclaimer</a></li>

                                        </ul>
                                    </div>
                                </li>
								<li class="visible-md visible-lg nav-visible v-divider"><a href="#" class="search-opener"><span class="icon icon-search"></span></a></li>
							</ul>
						</div>
					</nav>
				
				
				