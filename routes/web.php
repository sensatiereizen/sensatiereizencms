<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'site.pages.homepage');
Route::get('/', function()
{
    return view('site.pages.homepage');
})->name('home.show');

// Route::get('/', 'Site\CategoryController@links');

Route::get('/golfreizen/{slug}', 'Site\CategoryController@show')->name('category.show');
// Route::get('/category', 'Site\CategoryController@all')->name('category.show');
Route::get('/golfreis/{slug}', 'Site\ProductController@show')->name('product.show');
Route::get('/voetbalreizen/{slug}', 'Site\SoccerCategoryController@show')->name('soccercategory.show');
Route::get('/voetbalreizen', 'Site\SoccerCategoryController@showmain')->name('soccercategorypage.showmain');
Route::get('/golfreizen', 'Site\SoccerCategoryController@showgolfmain')->name('golfcategorypage.showgolfmain');
// Route::get('/category', 'Site\CategoryController@showmain')->name('soccercategorypage.showmain');
Route::get('/index-filtering', 'Site\CategoryController@indexFiltering')->name('category.');

Route::post('/offerte-aanvraag', 'Site\ProductController@addToCart')->name('product.add.cart');
Route::post('/offerte-aanvraag', 'Site\SoccerCategoryController@addToCart')->name('product.socceradd.cart');
Route::get('/cart', 'Site\CartController@getCart')->name('checkout.cart');
Route::get('/cart/item/{id}/remove', 'Site\CartController@removeItem')->name('checkout.cart.remove');
Route::get('/cart/clear', 'Site\CartController@clearCart')->name('checkout.cart.clear');
Route::get('/search', 'Site\ProductController@search')->name('search');

Route::get('/search-algolia', 'Site\ProductController@searchAlgolia')->name('search-algolia');
Route::get('/checkout', 'Site\CheckoutController@getCheckout')->name('checkout.index');
Route::post('/checkout/order', 'Site\CheckoutController@placeOrder')->name('checkout.place.order');

Route::get('send-mail', function () {

   

    $details = [

        'title' => 'Mail from ItSolutionStuff.com',

        'body' => 'This is for testing email using smtp'

    ];

   

    \Mail::to('mitch@sensatiereizen.nl')->send(new \App\Mail\MyTestMail($details));

   

    dd("Email is Sent.");

});

Route::group(['middleware' => ['auth']], function () {
  

    Route::get('checkout/payment/complete', 'Site\CheckoutController@complete')->name('checkout.payment.complete');

    Route::get('account/orders', 'Site\AccountController@getOrders')->name('account.orders');
});

Auth::routes();
require 'admin.php';
